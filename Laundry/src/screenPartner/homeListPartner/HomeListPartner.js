import React, { useState, useEffect } from 'react';
import { Text, SafeAreaView, StyleSheet, StatusBar, View, TouchableOpacity, FlatList, ImageBackground } from 'react-native'
import { Title, List, Paragraph, } from 'react-native-paper';
import { connect } from 'react-redux';
import { useFocusEffect } from '@react-navigation/native';
import { ShoworderInPartner, host } from '../../api/index'
import Hamburger from 'react-native-animated-hamburger';


const HomeListPartner = ({ navigation, tokenUser }) => {
    const [dataOrder, setdaPartner] = useState([])
    const [isloading, setIsloading] = useState(false)
    const [Service, setService] = useState(20)

    useEffect(() => {
        GetOrderPartner()
    }, [])

    useFocusEffect(
        React.useCallback(() => {
            GetOrderPartner()
        }, [])
    );

    const GetOrderPartner = () => {
        ShoworderInPartner(tokenUser)
            .then(response => {
                setdaPartner(response.data)
                console.log('Test data store data ', response.data)
            })
            .catch(error => {
                console.log('eror', JSON.stringify(error))
            })
    }

    const myRendernumber = (item, index) => {
        console.log('test', item)
        const total = item.Price + item.Dry + item.Detergent + item.Fabricsof + Service
        return (
            <TouchableOpacity
                onPress={() => navigation.navigate('ListDetail', { sentId: item.Ordersid, sentNameStore: item.Storename })}
            >
                <List.Section >
                    <List.Item
                        left={() => (
                            <ImageBackground style={styles.imageAllStore}
                                imageStyle={{ borderRadius: 8 }}
                                source={{ uri: item.Filename != null ? item.Filename : 'https://f.ptcdn.info/077/060/000/pg22y32nzpJerZ6Zocwl-o.jpg' }}
                            >
                                <View style={{ backgroundColor: '#FFC224', height: 30, width: 50, borderBottomRightRadius: 13, borderTopRightRadius: 13, marginTop: '5%' }}>
                                    <Text style={styles.ViewText1}>20 Km.</Text>
                                </View>
                                <View style={styles.flatlistView1} >
                                    <View >
                                        <Title style={styles.flatlistView2}> {total}THB.</Title>
                                    </View>
                                    <View >
                                        <Paragraph style={styles.TitleName}>{item.Storename}</Paragraph>
                                    </View>
                                </View>
                            </ImageBackground>
                        )}
                    />
                </List.Section>
            </TouchableOpacity>
        )
    }

    const workingRefresh = () => {
        setIsloading(true)
        GetOrderPartner()
        setTimeout(() => {
            setIsloading(false)
        }, 2000)
    }

    return (
        <SafeAreaView style={styles.safearea} forceInset={{ top: 'always' }}  >
            <StatusBar translucent={true} backgroundColor={'transparent'} barStyle="dark-content" />
            <View style={styles.Viewflex1}  >
                <ImageBackground style={styles.blackground}
                    source={{ uri: 'https://i.pinimg.com/originals/95/71/78/957178a0259dec1a3fec620428beadf1.gif' }}
                >
                    <View style={{ flex: 0, flexDirection: 'column', justifyContent: 'center', alignItems: 'center', }}>
                        <View style={{
                            flex: 0,
                            zIndex: 0,
                            marginTop: 24,
                            marginRight: 300
                        }}>
                            <Hamburger type="cross"
                                onPress={() => { navigation.openDrawer(); }
                                }
                                underlayColor="transparent"
                            >
                            </Hamburger>
                        </View>
                        <View style={{ flex: 0, flexDirection: 'column', justifyContent: 'center', alignItems: 'center', }}>
                            <Title>My loaction</Title>
                            <Text style={{ fontSize: 16 }}>Chaingmai,Thailand</Text>
                        </View>
                    </View>
                </ImageBackground>
            </View>
            <View style={styles.Viewflex4}  >
                {!dataOrder ?
                    <View style={{ width: "100%", height: "100%", backgroundColor: 'white', flexDirection: 'column', justifyContent: 'center', alignItems: 'center', }} >
                        <ImageBackground style={{ width: 160, height: 90, backgroundColor: 'white', }}
                            source={{ uri: 'https://www.geckoboard.com/uploads/analysis-guide-illo-2.png' }}
                        >
                        </ImageBackground>
                        <Title style={{ margin: '4%', color: '#CBCBCB' }}>Your order is lonely</Title>
                    </View>
                    :
                    <FlatList
                        data={dataOrder}
                        renderItem={({ item }) => myRendernumber(item)}
                        keyExtractor={(item, index) => index + ''}
                        refreshing={isloading}
                        onRefresh={workingRefresh}
                        ListHeaderComponent={
                            <Title style={{ marginTop: 24, marginLeft: 5 }}   >Order</Title>
                        }
                    >
                    </FlatList>
                }
            </View>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    safearea: {
        flex: 1
    },
    Viewflex1: {
        flex: 0,
        flexDirection: 'row',
        zIndex: 0,
    },
    Viewflex2: {
        flex: 0,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
    },
    Viewflex4: {
        flex: 2,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        marginLeft: 8
    },
    blackground: {
        flexDirection: 'column',
        width: 360,
        height: 165
    },
    image: {
        height: 60,
        width: 60,
        margin: 8,
        borderRadius: 40,
    },
    flatlistView1: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
        backgroundColor: '#C4C4C4',
        opacity: 0.9,
        marginTop: 70,
        marginLeft: 0.01,
        marginRight: 150,
    },
    flatlistView2: {
        color: 'white',
        marginLeft: 5
    },
    ViewText1: {
        color: 'white',
        marginLeft: 0,
        marginTop: 5
    },
    imageAllStore: {
        height: 180,
        width: 330,
        borderRadius: 40,
    },
    TitleName: {
        color: 'white',
        marginLeft: 10
    }

})

const mapStateToProps = state => {
    return {
        tokenUser: state.login.token,
    }
}

export default connect(mapStateToProps, null)(HomeListPartner)

