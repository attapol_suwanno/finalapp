import React, { useState, useEffect } from 'react';
import { View, ImageBackground, StatusBar, SafeAreaView, ScrollView, StyleSheet, TouchableOpacity, Text, Image } from 'react-native';
import { Avatar, Title, Button, Divider } from 'react-native-paper';
import { connect } from 'react-redux';
import { showdetailpartner, AcceptToOrederInPartner, host } from '../../api/index'


const ListDetail = ({ route, navigation, tokenUser }) => {
    const [Service, setService] = useState(20)
    const [setailOrder, setDetailOrder] = useState({})
    useEffect(() => {
        getdetilorder(route.params.sentId)
        onclickAccept()
    }, [])


    const getdetilorder = (id) => {
        showdetailpartner(id, tokenUser)
            .then(response => {
                setDetailOrder(response.data)
                console.log('data detail : ', response.data)
            })
            .catch(error => {
                console.log('eror', JSON.stringify(error))
            })
    }

    const onclickAccept = () => {
        AcceptToOrederInPartner(setailOrder.Ordersid, tokenUser)
            .then(response => {
                navigation.navigate('ListTransection')
                console.log('data detail : ', response.data)
            })
            .catch(error => {
                console.log('eror', JSON.stringify(error))
            })
    }
    const TotalPrice = setailOrder.Price + setailOrder.Dry + setailOrder.Detergent + setailOrder.Fabricsof + Service
    return (
        <ScrollView contentContainerStyle={{ flexGrow: 1 }}>
            <StatusBar translucent={true} backgroundColor={'transparent'} barStyle="dark-content" />
            <ImageBackground style={styles.backgroundMain}
                source={{ uri: setailOrder.Filename != null ? setailOrder.Filename : 'https://f.ptcdn.info/077/060/000/pg22y32nzpJerZ6Zocwl-o.jpg' }}
            >
                <SafeAreaView style={styles.sefeArea} forceInset={{ top: 'always' }}  >
                    <View style={{ flex: 1, flexDirection: 'column', }}  >
                        <TouchableOpacity onPress={() => navigation.goBack()}  >
                            <Avatar.Icon size={60} icon="chevron-left" style={{ backgroundColor: 'transparent', marginTop: 14 }} />
                        </TouchableOpacity>
                        <View style={styles.locationflex}>
                            <View>
                                <Title style={styles.nameStore} textStyle={{ flexWrap: 'wrap' }} >{setailOrder.Storename}</Title>
                            </View>
                            <View style={styles.locationIcon}>
                                <View>
                                    <Avatar.Icon size={70} icon="map-marker" style={{ backgroundColor: 'transparent', marginTop: 0 }} />
                                </View>
                                <View>
                                    <Title style={styles.locationText}>Location</Title>
                                    <Title style={styles.TextKm}>0.23KM.</Title>
                                </View>
                            </View>
                        </View>
                    </View>
                </SafeAreaView>
            </ImageBackground>
            <View style={styles.flex5}
            >
                <Title style={styles.TextTitle}>Order information</Title>
                <View style={styles.flex8}>
                    <View style={styles.flex9}  >
                        <Text style={styles.Text}>Wash</Text>
                        <Text style={styles.Text}>Dry</Text>
                        <Text style={styles.Text}>Laundry  Detergent</Text>
                        <Text style={styles.Text}>Fabric Softener</Text>
                    </View>
                    <View style={styles.flex10}  >
                        <Text style={styles.Text}>THB{setailOrder.Price}</Text>
                        <Text style={styles.Text}>THB{setailOrder.Dry}</Text>
                        <Text style={styles.Text}>THB{setailOrder.Detergent}</Text>
                        <Text style={styles.Text}>THB{setailOrder.Fabricsof}</Text>
                    </View>
                </View>
                <Divider style={styles.Text} />
                <View style={styles.flex8}>
                    <View style={styles.flex9}  >
                        <Text style={styles.Text}>Service</Text>
                        <Text style={styles.Text}>Total</Text>
                    </View>
                    <View style={styles.flex10}  >
                        <Text style={styles.Text}>THB{Service}</Text>
                        <Text style={styles.Text}>THB{TotalPrice}</Text>
                    </View>
                </View>
                <View style={styles.flex11}  >
                    <Button style={styles.button} color="#66B5F8" icon="logout-variant" mode="contained" onPress={() => onclickAccept()} >
                        <Text> Accept</Text>
                    </Button>
                </View>
            </View>
        </ScrollView>
    )
}

const styles = StyleSheet.create({
    backgroundMain: {
        width: 360,
        height: 235
    },
    sefeArea: {
        flex: 1,
        flexDirection: 'column'
    },
    flex4: {
        padding: '3%'
    },
    flex5: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: "white",
        paddingHorizontal: 32
    },
    flex6: {
        flexDirection: 'row',
        marginTop: 16
    },
    flex7: {
        flexDirection: 'column',
        backgroundColor: "white",
    },
    flex8: {
        flexDirection: 'row'
    },
    flex9: {
        flex: 1,
    },
    flex10: {
        flex: 0,
        flexDirection: 'column',
        alignItems: 'flex-end',
    },
    flex11: {
        alignItems: 'center',
        marginTop: 32
    },
    button: {
        zIndex: 0,
        borderRadius: 25,
        width: 135,
        height: 30,
        justifyContent: 'center',
        elevation: 25,
        height: 45
    },
    absolute: {
        position: "absolute",
        top: 0,
        left: 0,
        bottom: 0,
        right: 0
    },
    TitleName: {
        borderWidth: 1,
        backgroundColor: 'white',
        borderColor: 'white',
        padding: '1.5%',
        borderRadius: 15
    },
    Text: {
        marginTop: 16
    },
    TextTitle: {
        marginTop: 32
    },
    nameStore: {
        marginTop: 50,
        color: 'white',
        fontSize: 24
    },
    locationflex: {
        flexDirection: 'column',
        width: 400,
        marginLeft: 16
    },
    locationIcon: {
        flexDirection: 'row',
    },
    locationText: {
        fontSize: 14,
        color: 'white',
        padding: 0
    },
    TextKm: {
        color: 'white',
        marginTop: 0,
        fontSize: 22,
    }
})

const mapStateToProps = state => {
    return {
        tokenUser: state.login.token,
    }
}

const mapDispatchToProps = {
}

export default connect(mapStateToProps, mapDispatchToProps)(ListDetail)
