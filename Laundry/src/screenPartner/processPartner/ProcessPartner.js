import React, { useState, useEffect, useRef } from 'react';
import MapView, { PROVIDER_GOOGLE, Marker } from 'react-native-maps'; // remove PROVIDER_GOOGLE import if not using Google Maps
import { View, StatusBar, SafeAreaView, ScrollView, StyleSheet, Image, Text, TouchableOpacity, Linking } from 'react-native';
import { Title, Avatar,Divider } from 'react-native-paper';
import { ProgressSteps, ProgressStep } from 'react-native-progress-steps';
import { updateStepFinishOrder, ShowOrderDetailPartner, getLocationStore, host } from '../../api/index'
import { connect } from 'react-redux';

const ProcessPartner = ({ route, navigation, tokenUser }) => {
  const [storeData, setStoreData] = useState({})
  const [storeLocation, setStoreLocation] = useState({})
  const Service = 20
  const MapViewRef = useRef()

  useEffect(() => {
    listOrderdetailPartner(route.params.idOrder)
    MapViewRef.current.animateCamera({
      center: {
        latitude: 18.791115,
        longitude: 98.955375,
      }
    })
  }, [])


  const listOrderdetailPartner = (id) => {
    ShowOrderDetailPartner(id, tokenUser)
      .then(response => {
        setStoreData(response.data)
        storeLocationD(response.data.Storeid)
        console.log(' id Store  : ', response.data.Storeid)
        console.log(' list orderdetail : ', response.data)
      })
      .catch(error => {
        console.log('eror', JSON.stringify(error))
      })
  }


  const storeLocationD = (id) => {
    getLocationStore(id)
      .then(response => {
        setStoreLocation(response.data)
        console.log(' Detail Store : ', response.data)
      })
      .catch(error => {
        console.log('eror', JSON.stringify(error))
      })
  }

  const progressStepsStyle = {
    activeStepIconBorderColor: '#66B5F8',
    activeLabelColor: '#66B5F8',
    activeStepNumColor: '#66B5F8',
    activeStepIconColor: 'transparent',
    completedStepIconColor: '#66B5F8',
    completedProgressBarColor: '#B3DAFB',
    completedCheckColor: 'white'
  };


  const onSubmitSteps = () => {
    updateStepFinishOrder(route.params.idOrder, tokenUser)
      .then(response => {
        navigation.navigate('HomeListPartner')
        console.log('finish step : ', response.data)
      })
      .catch(error => {
        console.log('eror', JSON.stringify(error))
      })
  };


  const defaultScrollViewProps = {
    keyboardShouldPersistTaps: 'handled',
    contentContainerStyle: {
      flex: 1,
      justifyContent: 'center'
    }
  };

  const buttonTextStyle = {
    color: '#686868',
    fontWeight: 'bold'
  };
  const TotalPrice = storeData.Price + storeData.Dry + storeData.Detergent + storeData.Fabricsof + Service
  return (
    <ScrollView contentContainerStyle={{ flexGrow: 1 }}>
      <StatusBar translucent={true} backgroundColor={'transparent'} barStyle="dark-content" />
      <SafeAreaView style={{ flex: 1, }} forceInset={{ top: 'always' }}  >
        <View style={{ flex: 1, flexDirection: 'column', justifyContent: 'center', alignItems: 'center', marginTop: '5%' }}  >

          <ProgressSteps  {...progressStepsStyle}>
            <ProgressStep label="Reciving" nextBtnTextStyle={styles.buttonTextStyle} previousBtnTextStyle={styles.buttonTextStyle}  >
              <View style={{ flex: 1, flexDirection: 'column', }}  >
                <View style={styles.flex6}  >
                  <View style={styles.flex4}  >
                    <Image
                      style={{ width: 52, height: 52, marginRight: 10, borderRadius: 50 }}
                      source={{
                        uri: "https://images.unsplash.com/photo-1573094116942-ef6a160e2267?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=60",
                      }} />
                  </View>
                  <View style={styles.flex4}  >
                    <Text>{storeData.Username}</Text>
                    <Text style={{ color: 'blue' }}
                      onPress={() => Linking.openURL(`tel:${storeData.Phone}`)}>
                      {storeData.Phone}
                    </Text>
                  </View>
                  <View style={styles.flex5}  >
                    <TouchableOpacity style={{ zIndex: 1, }} onPress={() => Linking.openURL(`tel:${storeData.Phone}`)}  >
                      <Avatar.Icon size={40} icon="phone" style={{ backgroundColor: '#66B5F8' }} />
                    </TouchableOpacity>
                  </View>
                </View>
                <Divider />
                <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center', padding: 4 }} >
                  <Title>THB{TotalPrice} </Title>
                  <View style={{ backgroundColor: '#E5E5E5', padding: 4, borderRadius: 8 }}>
                    <Text> CASH</Text>
                  </View>
                </View>
              </View>
              <View style={{ flex: 1, width: 400, height: 300 }}  >
                <View style={styles.container}>
                  <MapView
                    ref={MapViewRef}
                    provider={PROVIDER_GOOGLE} // remove if not using Google Maps
                    style={styles.map}
                    region={{
                      latitude: 18.791115,
                      longitude: 98.955375,
                      latitudeDelta: 0.015,
                      longitudeDelta: 0.0121,
                    }}
                  >
                    <Marker draggable
                      coordinate={{
                        latitude: 18.791115,
                        longitude: 98.955375,
                      }}
                    />
                  </MapView>
                </View>
              </View>
            </ProgressStep>
            <ProgressStep label="Go to store">
              <View style={{ flex: 1, flexDirection: 'column', }}  >
                <View style={styles.flex6}  >
                  <View style={styles.flex4}  >
                    <Image
                      style={{ width: 52, height: 52, marginRight: 10, borderRadius: 50 }}
                      source={{
                        uri: storeData.Filename,
                      }} />
                  </View>
                  <View style={styles.NameStoreLocation}  >
                    <Title>{storeData.Storename}</Title>
                  </View>
                </View>
              </View>
              <View style={{ flex: 1, width: 400, height: 300 }}  >
                <View style={styles.container}>
                  {!storeLocation === null ?
                    null
                    :
                    <MapView
                      ref={MapViewRef}
                      provider={PROVIDER_GOOGLE} // remove if not using Google Maps
                      style={styles.map}
                      region={{
                        latitude: 18.791115,
                        longitude: 98.955375,
                        latitudeDelta: 0.015,
                        longitudeDelta: 0.0121,
                      }}
                    >
                      <Marker
                        coordinate={{
                          latitude: storeLocation.Latitude,
                          longitude: storeLocation.Longitude,
                        }}
                        centerOffset={{ x: -18, y: -60 }}
                        anchor={{ x: 0.69, y: 1 }}
                      >
                        {!storeData.Filename ?
                          null
                          :
                          <Image source={{ uri: storeData.Filename }} style={{ height: 35, width: 35, borderRadius: 25, }} />
                        }

                      </Marker>
                    </MapView>
                  }
                </View>
              </View>
            </ProgressStep>
            <ProgressStep label="Luandry">
              <View style={{ flex: 1, flexDirection: 'column', }}  >
                <View style={styles.flex6}  >
                  <View style={styles.flex4}  >
                    <Image
                      style={{ width: 52, height: 52, marginRight: 10, borderRadius: 50 }}
                      source={{
                        uri: "https://images.unsplash.com/photo-1573094116942-ef6a160e2267?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=60",
                      }} />
                  </View>
                  <View style={styles.flex4}  >
                    <Text>{storeData.Username}</Text>
                    <Text style={{ color: 'blue' }}
                      onPress={() => Linking.openURL(`tel:${storeData.Phone}`)}>
                      {storeData.Phone}

                    </Text>
                  </View>
                  <View style={styles.flex5}  >
                    <TouchableOpacity style={{ zIndex: 1, }} onPress={() => Linking.openURL(`tel:${storeData.Phone}`)} >
                      <Avatar.Icon size={40} icon="phone" style={{ backgroundColor: '#66B5F8' }} />
                    </TouchableOpacity>
                  </View>
                </View>
                <Divider />
                <Title style={styles.TextTitle}>Order information</Title>
                <View style={styles.flex8}>
                  <View style={styles.flex9}  >
                    <Text style={styles.Text}>Wash</Text>
                    <Text style={styles.Text}>Dry</Text>
                    <Text style={styles.Text}>Laundry  Detergent</Text>
                    <Text style={styles.Text}>Fabric Softener</Text>
                  </View>
                  <View style={styles.flex10}  >
                    <Text style={styles.Text}>THB{storeData.Price}</Text>
                    <Text style={styles.Text}>THB{storeData.Dry}</Text>
                    <Text style={styles.Text}>THB{storeData.Detergent}</Text>
                    <Text style={styles.Text}>THB{storeData.Fabricsof}</Text>
                  </View>
                </View>
              </View>
            </ProgressStep >
            <ProgressStep label="Finish" onSubmit={onSubmitSteps}>
              <View style={{ flex: 1, flexDirection: 'column', }}  >
                <View style={styles.flex6}  >
                  <View style={styles.flex4}  >
                    <Image
                      style={{ width: 52, height: 52, marginRight: 10, borderRadius: 50 }}
                      source={{
                        uri: 'https://i.pinimg.com/474x/7a/19/05/7a19052cf64453fce4d95858b5925e58.jpg',
                      }} />
                  </View>
                  <View style={styles.flex4}  >
                    <Text>{storeData.Username}</Text>
                    <Text style={{ color: 'blue' }}
                      onPress={() => Linking.openURL(`tel:${storeData.Phone}`)}>
                      {storeData.Phone}
                    </Text>
                  </View>
                  <View style={styles.flex5}  >
                    <TouchableOpacity style={{ zIndex: 1, }} onPress={() => Linking.openURL(`tel:${storeData.Phone}`)}  >
                      <Avatar.Icon size={40} icon="phone" style={{ backgroundColor: '#66B5F8' }} />
                    </TouchableOpacity>
                  </View>
                </View>
                <Divider />
                <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center', padding: 4 }} >
                  <Title>THB{TotalPrice} </Title>
                  <View style={{ backgroundColor: '#E5E5E5', padding: 4, borderRadius: 8 }}>
                    <Text> CASH</Text>
                  </View>
                </View>
              </View>
              <View style={{ flex: 1, width: 400, height: 300 }}  >
                <View style={styles.container}>
                  <MapView
                    ref={MapViewRef}
                    provider={PROVIDER_GOOGLE} // remove if not using Google Maps
                    style={styles.map}
                    region={{
                      latitude: 18.791115,
                      longitude: 98.955375,
                      latitudeDelta: 0.015,
                      longitudeDelta: 0.0121,
                    }}
                  >

                    <Marker draggable
                      coordinate={{
                        latitude: 18.791115,
                        longitude: 98.955375,
                      }}

                    />
                  </MapView>
                </View>
              </View>
            </ProgressStep>
          </ProgressSteps>
        </View>
      </SafeAreaView>
    </ScrollView>
  )
}

const styles = StyleSheet.create({
  buttonTextStyle: {

  },
  container: {
    ...StyleSheet.absoluteFillObject,
    height: '100%',
    width: '100%',
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  map: {
    ...StyleSheet.absoluteFillObject,
  },
  button: {
    zIndex: 0,
    borderRadius: 25,
    width: "60%",
    height: "100%",
    justifyContent: 'center',
    elevation: 25,
    height: 45
  },
  flex4: {
    marginLeft: 0.1
  },
  flex5: {
    marginLeft: 135,
    marginTop: 4
  },
  flex6: {
    flexDirection: 'row',
    padding: 8,
    marginLeft: 15
  },
  TitleName: {
    borderWidth: 1,
    backgroundColor: 'white',
    borderColor: 'white',
    padding: '1.5%',
    borderRadius: 15
  },
  Text: {
    marginTop: 16
  },
  TextTitle: {
    marginTop: 32,
    marginLeft: 24
  },
  flex8: {
    flexDirection: 'row',
    padding: 24
  },
  flex9: {
    flex: 1,
  },
  flex10: {
    flex: 0,
    flexDirection: 'column',
    alignItems: 'flex-end',
  },
  NameStoreLocation: {
    marginTop: 8,
    fontSize: 24
  }
});

const mapStateToProps = state => {
  return {
    tokenUser: state.login.token,
  }
}

export default connect(mapStateToProps, null)(ProcessPartner)
