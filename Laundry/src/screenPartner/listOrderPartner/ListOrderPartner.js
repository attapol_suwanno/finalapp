import React, { useState, useEffect } from 'react';
import { View, Text, ImageBackground, StatusBar, SafeAreaView, ScrollView, StyleSheet, Image, TouchableOpacity, Linking } from 'react-native';
import { Button, Title, Paragraph,  Avatar,  Divider } from 'react-native-paper';
import { connect } from 'react-redux';
import { useFocusEffect } from '@react-navigation/native';
import { ShowOrderDetailPartner } from '../../api/index'


const ListOrderPartner = ({ route, navigation, tokenUser }) => {
    const [isloading, setIsloading] = useState(false)
    const [storeDataAll, setStoreData] = useState({})
    const [Service, setService] = useState(20)
    const [TotalPrice, setTotalPrice] = useState(0)

    useEffect(() => {
        listOrderdetailPartner(route.params.orderID)
    }, [])

    useFocusEffect(
        React.useCallback(() => {
            listOrderdetailPartner(route.params.orderID)
        }, [])
    );

    const listOrderdetailPartner = (id) => {
        setIsloading(true)
        ShowOrderDetailPartner(id, tokenUser)
            .then(response => {
                setStoreData(response.data)
                console.log(' list orderdetail : ', response.data)
                setIsloading(false)
            })
            .catch(error => {
                console.log('eror', JSON.stringify(error))
            })
    }

    const checkHaveData = () => {
        if (storeDataAll != null) {
            return (
                <View>
                    <ImageBackground style={styles.backgroundMain}
                        source={{ uri: storeDataAll.Filename }}
                    >
                        <View style={{ flex: 1, flexDirection: 'column', marginLeft: '5%' }}  >
                            <TouchableOpacity onPress={() => navigation.goBack()}  >
                                <Avatar.Icon size={60} icon="chevron-left" style={{ backgroundColor: 'transparent', marginTop: 14 }} />
                            </TouchableOpacity>
                            <View style={styles.locationflex}>
                                <View>
                                    <Title style={styles.nameStore} textStyle={{ flexWrap: 'wrap' }} >{storeDataAll.Storename}</Title>
                                </View>
                                <View style={styles.locationIcon}>
                                    <View>
                                        <Avatar.Icon size={70} icon="map-marker" style={{ backgroundColor: 'transparent', marginTop: 0 }} />
                                    </View>
                                    <View>
                                        <Title style={styles.locationText}>Location</Title>
                                        <Title style={styles.TextKm}>0.23KM.</Title>
                                    </View>
                                </View>
                            </View>
                        </View>
                    </ImageBackground>
                    <View style={styles.flex5}
                    >
                        <Title style={styles.TextTitle}>User information</Title>
                        <View style={styles.flex7}  >
                            <View style={styles.flex6}  >
                                <View style={styles.flex4}  >
                                    <Image
                                        style={{ width: 52, height: 52, marginRight: 10, borderRadius: 50 }}
                                        source={{
                                            uri: 'https://images.unsplash.com/photo-1573094116942-ef6a160e2267?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=60',
                                        }} />
                                </View>
                                <View style={styles.flex4}  >
                                    <Text>{storeDataAll.Username}</Text>
                                    <Text>{storeDataAll.Address}</Text>
                                    <Text style={{ color: 'blue' }}
                                        onPress={() => Linking.openURL(`tel:${storeDataAll.Phone}`)}>
                                        {storeDataAll.Phone}
                                    </Text>
                                </View>
                            </View>
                        </View>
                        <Title style={styles.TextTitle}>Order information</Title>
                        <View style={styles.flex8}>
                            <View style={styles.flex9}  >
                                <Text style={styles.Text}>Wash</Text>
                                <Text style={styles.Text}>Dry</Text>
                                <Text style={styles.Text}>Laundry  Detergent</Text>
                                <Text style={styles.Text}>Fabric Softener</Text>
                            </View>
                            <View style={styles.flex10}  >
                                <Text style={styles.Text}>THB{storeDataAll.Price}</Text>
                                <Text style={styles.Text}>THB{storeDataAll.Dry}</Text>
                                <Text style={styles.Text}>THB{storeDataAll.Detergent}</Text>
                                <Text style={styles.Text}>THB{storeDataAll.Fabricsof}</Text>
                            </View>
                        </View>
                        <Divider style={styles.Text} />
                        <View style={styles.flex8}>
                            <View style={styles.flex9}  >
                                <Text style={styles.Text}>Service</Text>
                                <Text style={styles.Text}>Total</Text>
                            </View>
                            <View style={styles.flex10}  >
                                <Text style={styles.Text}>THB{Service}</Text>
                                <Text style={styles.Text}>THB{TotalPrice}</Text>
                            </View>
                        </View>
                        <View style={styles.flex11}  >
                            <Button style={styles.button} color="#66B5F8" icon="logout-variant" mode="contained" onPress={() => navigation.navigate('ProcessPartner', { idOrder: route.params.orderID },)}   >
                                <Text> Start</Text>
                            </Button>
                        </View>
                    </View>
                </View>
            )
        } else {
            return (
                <View style={styles.ViewBig}  >
                    <Image
                        style={styles.photo}
                        source={{
                            uri: 'https://image.freepik.com/free-photo/front-view-pile-laundry_23-2148387001.jpg'
                        }} />
                    <Title>คุณไม่มีงานค้างใดๆเหลือแล้ว</Title>
                    <Paragraph>กรุณากลับมาที่ศูนย์กลางในโซนของคุณ</Paragraph>
                </View>
            )
        }
    }

    return (
        <ScrollView contentContainerStyle={{ flexGrow: 1 }}>
            <ImageBackground style={{ flex: 1, backgroundColor: 'white' }}
            >
                <SafeAreaView style={{ flex: 1, }} forceInset={{ top: 'always' }}  >
                    <StatusBar translucent={true} backgroundColor={'transparent'} barStyle="dark-content" />
                    {checkHaveData()}
                </SafeAreaView>
            </ImageBackground>
        </ScrollView>
    )
}

const styles = StyleSheet.create({
    backgroundMain: {
        width: 360,
        height: 235
    },
    button: {
        zIndex: 0,
        borderRadius: 25,
        width: "60%",
        height: "100%",
        justifyContent: 'center',
        elevation: 25,
        height: 45
    },
    flexRow1: {
        flex: 1,
    },
    flexRow3: {
        flex: 0,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginLeft: '5%',
        marginRight: '5%',
        marginTop: '5%'
    },
    flex4: {
        padding: '3%'
    },
    flex5: {
        flexDirection: 'column',
        backgroundColor: "white",
        paddingHorizontal: 32
    },
    flex6: {
        flexDirection: 'row',
        marginTop: 16
    },
    flex7: {
        flexDirection: 'column',
        backgroundColor: "white",
    },
    flex8: {
        flexDirection: 'row'
    },
    flex9: {
        flex: 1,
    },
    flex10: {
        flex: 0,
        flexDirection: 'column',
        alignItems: 'flex-end',
    },
    flex11: {
        flex: 0,
        alignItems: 'center',
        marginTop: 32,
        marginBottom: 15
    },
    button: {
        zIndex: 0,
        borderRadius: 25,
        width: 135,
        height: 30,
        justifyContent: 'center',
        elevation: 25,
        height: 45
    },
    photo: {
        width: 100,
        height: 100,
        borderRadius: 50,
    },
    ViewBig: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
    },
    TitleName: {
        borderWidth: 1,
        backgroundColor: 'white',
        borderColor: 'white',
        padding: '1.5%',
        borderRadius: 15
    },
    Text: {
        marginTop: 16
    },
    TextTitle: {
        marginTop: 32
    },
    nameStore: {
        marginTop: 50,
        color: 'white',
        fontSize: 24
    },
    locationflex: {
        flexDirection: 'column',
        width: 400,
        marginLeft: 16
    },
    locationIcon: {
        flexDirection: 'row',
    },
    locationText: {
        fontSize: 14,
        color: 'white',
        padding: 0
    },
    TextKm: {
        color: 'white',
        marginTop: 0,
        fontSize: 22,
    }
});

const mapStateToProps = state => {
    return {
        tokenUser: state.login.token,
    }
}
export default connect(mapStateToProps, null)(ListOrderPartner)

