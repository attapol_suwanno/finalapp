import React, { useState, useRef, useEffect } from 'react';
import { Text, SafeAreaView, StyleSheet, StatusBar, View, Dimensions, TouchableOpacity, FlatList, ImageBackground } from 'react-native'
import { Searchbar, Title, List, Avatar, Colors } from 'react-native-paper';
import Carousel, { ParallaxImage } from 'react-native-snap-carousel';
import { useFocusEffect } from '@react-navigation/native';
import { connect } from 'react-redux';
import { ListStoreAll } from '../../api/index'
const { width: screenWidth } = Dimensions.get('window')

const Home = ({ navigation }) => {
    const carouselRef = useRef(null)
    const [searchData, setSearchData] = useState('')
    const [storeDataAll, setStoreDataAll] = useState([])
    const [isloading, setIsloading] = useState(false)
    const [callReached, setCallReached] = useState()
    const [offsetValue, setOffsetValue] = useState(0)

    useEffect(() => {
        StoreAll()
    }, [])

    useFocusEffect(
        React.useCallback(() => {
            StoreAll()
        }, [])
    );

    const StoreAll = (data) => {
        setIsloading(true)
        ListStoreAll(data)
            .then(response => {
                setStoreDataAll(response.data)
                console.log('Test data store data ', response.data)
                setIsloading(false)
            })
            .catch(error => {
                console.log('eror', JSON.stringify(error))
            })
    }

    const _renderItem = ({ item, index }, parallaxProps) => {
        return (
            <View>
                {!item.Promotion ?
                    null
                    :
                    <TouchableOpacity onPress={() => navigation.navigate('StoreDetail', { sentId: item.ID, sentData: item })}>
                        <View style={styles.item}>
                            {!item.Promotion ?
                                null
                                :
                                <ParallaxImage
                                    source={{ uri: item.Promotion }}
                                    containerStyle={styles.imageContainer}
                                    style={styles.image}
                                    parallaxFactor={0.1}
                                    {...parallaxProps}

                                />
                            }
                            <Text style={styles.title} numberOfLines={2}>
                                {item.store_Name}
                            </Text>
                        </View>
                    </TouchableOpacity>
                }
            </View>
        )
    }

    const onUseloadMore = () => {
        setOffsetValue(parseInt(offsetValue) + parseInt(10))
    }

    const myRendernumber = (item, index) => {
        console.log('dataget api: ', item)
        return (
            <TouchableOpacity
                onPress={() => navigation.navigate('StoreDetail', { sentId: item.ID, sentData: item })}
            >
                <List.Section >
                    <List.Item
                        left={() => (
                            <ImageBackground style={styles.imageAllStore}
                                imageStyle={{ borderRadius: 8 }}
                                source={{ uri: item.Filename }}
                            >
                                <View style={{ backgroundColor: '#FFC224', height: 30, width: 50, borderBottomRightRadius: 13, borderTopRightRadius: 13, marginTop: '5%' }}>
                                    <Text style={styles.ViewText1}>20 Km.</Text>
                                </View>
                                <View style={styles.flatlistView1} >
                                    <View style={styles.flatlistView2}>
                                        <Title style={styles.ViewText1}  >{item.Storename}</Title>
                                    </View>
                                </View>
                            </ImageBackground>
                        )}
                    />
                </List.Section>
            </TouchableOpacity>
        )
    }

    const workingRefresh = () => {
        setIsloading(true)
        StoreAll()
        setTimeout(() => {
            setIsloading(false)
        }, 2000)
    }

    return (
        !storeDataAll ?
            null
            :
            <SafeAreaView style={styles.safearea} forceInset={{ top: 'always' }}  >
                <StatusBar translucent={true} backgroundColor={'transparent'} barStyle="dark-content" />
                <ImageBackground style={styles.blackground}
                    source={{ uri: 'https://i.pinimg.com/originals/95/71/78/957178a0259dec1a3fec620428beadf1.gif' }}
                >
                    <View style={{ flex: 0, flexDirection: 'row', justifyContent: 'center', alignItems: 'center', }}>
                        <View style={{
                            flex: 1,
                            flexDirection: 'row',
                            justifyContent: 'flex-start',
                            alignItems: 'flex-start',
                            zIndex: 0,
                            marginTop: 32,
                            marginLeft: 16
                        }}>
                            <TouchableOpacity onPress={() => { navigation.openDrawer() }}>
                            <Avatar.Icon size={40} icon="menu" style={{ backgroundColor: Colors.white }} />
                            </TouchableOpacity>

                        </View>
                        <View style={{
                            flex: 1,
                            flexDirection: 'row',
                            justifyContent: 'flex-end',
                            alignItems: 'flex-start',
                            zIndex: 0,
                            marginTop: 32,
                            marginRight: 8
                        }}>
                            <TouchableOpacity onPress={() => navigation.navigate('MapAllStore')} >
                                <Avatar.Icon size={40} icon="map" style={{ backgroundColor: Colors.white }} />
                            </TouchableOpacity>
                        </View>
                    </View>
                    <View style={{ flex: 0, flexDirection: 'column', justifyContent: 'center', alignItems: 'center', }}>
                        <Title>My loaction</Title>
                        <Text style={{ fontSize: 16 }}>Chaingmai,Thailand</Text>
                    </View>
                </ImageBackground>
                <View style={styles.Viewflex2}  >
                    <Searchbar
                        placeholder="Search"
                        onChangeText={(query) => {
                            setSearchData(query)
                            StoreAll(query)
                        }}
                        style={styles.search}
                    />
                </View>
                <View style={styles.Viewflex4}  >
                    <FlatList
                        data={storeDataAll}
                        renderItem={({ item }) => myRendernumber(item)}
                        keyExtractor={(item, index) => index + ''}
                        refreshing={isloading}
                        onRefresh={workingRefresh}
                        onMomentumScrollBegin={() => setCallReached(true)}
                        onEndReached={() => {
                            if (callReached) {
                                onUseloadMore()
                            }
                            setCallReached(false)
                        }}
                        onEndReachedThreshold={0.01}
                        ListHeaderComponent={
                            <View>
                                {searchData != "" ?
                                    null
                                    :
                                    <View>
                                        <Title style={styles.Title1}  >Promotions</Title>
                                        <View style={styles.Viewflex3}  >
                                            <Carousel
                                                ref={carouselRef}
                                                sliderWidth={screenWidth}
                                                sliderHeight={screenWidth}
                                                itemWidth={screenWidth - 160}
                                                data={storeDataAll}
                                                renderItem={_renderItem}
                                                hasParallaxImages={true}
                                            />
                                        </View>
                                        <Title style={styles.Title2}>Allstore</Title>
                                    </View>
                                }

                            </View>
                        }
                    >
                    </FlatList>
                </View>
            </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    safearea: {
        flex: 1
    },
    search: {
        borderRadius: 30,
        flexDirection: 'row',
        position: 'absolute',
        zIndex: 2,
        justifyContent: 'center',
        alignItems: 'center',
        marginLeft: '5%',
        marginRight: '5%',
        height: 40,
        width: 294
    },
    Viewflex2: {
        flex: 0,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
    },
    Viewflex3: {
        flex: 0,
        flexDirection: 'column',
        justifyContent: 'center',
        marginTop: 16
    },
    Viewflex4: {
        flex: 2,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        marginLeft: 8,
        marginTop: '10%'
    },
    blackground: {
        flexDirection: 'column',
        width: 360,
        height: 165
    },
    container: {
        flex: 1,
    },
    content: {
        padding: 4,
    },

    item: {
        width: screenWidth - 173,
        height: screenWidth - 250,
    },
    imageContainer: {
        flex: 1,
        marginBottom: Platform.select({ ios: 0, android: 1 }), // Prevent a random Android rendering issue
        backgroundColor: 'white',
        borderRadius: 8,
    },
    image: {
        ...StyleSheet.absoluteFillObject,
        resizeMode: 'cover',
    },
    imageAllStore: {
        height: 180,
        width: 330,
        borderRadius: 40,
    },
    flatlistView1: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
    },
    flatlistView2: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: '15%'
    },
    ViewText1: {
        color: 'white',
        alignItems: 'center',
        justifyContent: 'center',
    },
    title: {
        marginTop: 0
    },
    Title1: {
        fontSize: 16,
        alignItems: 'flex-start',
        marginTop: 38,
        marginLeft: 8
    },
    Title2: {
        fontSize: 16,
        alignItems: 'flex-start',
        marginTop: 16,
        marginLeft: 8
    }
})

const mapStateToProps = state => {
    return {
        tokenUser: state.login.token,
    }
}

export default connect(mapStateToProps, null)(Home)

