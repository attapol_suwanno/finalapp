
import React, { useEffect } from 'react';
import { View, Text, ImageBackground, StatusBar, SafeAreaView, ScrollView, StyleSheet, Image, TouchableOpacity, } from 'react-native';
import { Button, Card, Divider } from 'react-native-paper';
import { connect } from 'react-redux';
import axios from 'axios'

const FirstUseApp = ({ route, navigation, tokenUser }) => {

    return (
        <ScrollView contentContainerStyle={{ flexGrow: 1 }}>
            <ImageBackground style={{ flex: 1, backgroundColor: 'white' }}
            >
                <SafeAreaView style={{ flex: 1, }} forceInset={{ top: 'always' }}  >
                    <StatusBar translucent={true} backgroundColor={'transparent'} barStyle="dark-content" />
                    <View style={{ flex: 1 }} >
                        <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'center', alignItems: 'center', marginLeft: '5%', marginRight: '5%', marginTop: '10%', }}  >
                            <Card.Cover source={ require('../../assets/logo.png') } style={{ width: 200, height: 200, borderTopLeftRadius: 100, borderTopRightRadius: 100, borderRadius: 100, elevation: 50, }} />
                        </View>
                        <View style={{ flex: 0, flexDirection: 'row', justifyContent: 'center', alignItems: 'center',margin:'1%' }}  >
                            <Text style={{fontSize:35}}>Loypha</Text>
                        </View>
                        <View style={{ flex: 0, flexDirection: 'row', justifyContent: 'center', alignItems: 'center', }}  >
                            <Text>Your life will be easier.</Text>
                        </View>
                        <Divider />
                        <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'center', alignItems: 'center', }}  >
                            <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center', marginLeft: '5%', marginRight: '5%' }}  >
                                <TouchableOpacity style={{ backgroundColor: 'white', borderWidth: 1, padding: 25, borderRadius: 8 , alignItems: 'center' }} onPress={() => navigation.navigate('mainRouter')}  >
                                    <Image
                                        style={styles.image1}
                                        source={
                                            require('../../assets/electronics.png')
                                        } />
                                        <Text>Luandry</Text>
                                </TouchableOpacity>
                            </View>
                            <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center', marginLeft: '5%', marginRight: '5%', }}  >
                                <TouchableOpacity style={{ backgroundColor: 'white', borderWidth: 1, padding: 25, borderRadius: 8 , alignItems: 'center'}} onPress={() => tokenUser != "" ? navigation.navigate('mainPartnerRouter') : navigation.navigate('LoginPartner')}  >
                                    <Image
                                        style={styles.image1}
                                        source={
                                            require('../../assets/food-and-restaurant.png')
                                        } />
                                        <Text>Delivery</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                </SafeAreaView>
            </ImageBackground>
        </ScrollView>
    )
}

const styles = StyleSheet.create({
    button: {
        zIndex: 0,
        borderRadius: 25,
        width: "60%",
        height: "100%",
        justifyContent: 'center',
        elevation: 25,
        height: 45

    }, image1: {
        width: 80,
        height: 80,
    },
});

const mapStateToProps = state => {
    return {
        tokenUser: state.login.token,
    }
}

export default connect(mapStateToProps, null)(FirstUseApp)

