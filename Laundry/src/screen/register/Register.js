import React, { useState, useCallback, useRef } from 'react';
import { View, Text, ImageBackground, StatusBar, SafeAreaView, ScrollView, StyleSheet, Image, TouchableOpacity } from 'react-native';
import { Button, TextInput, HelperText, Title, Colors, Avatar } from 'react-native-paper';
import { SignUpUser } from '../../api/index'

const Register = ({ route, navigation }) => {
    const [username, setUsername] = useState('')
    const [email, setEmail] = useState('')
    const [phonenumber, setphonenumber] = useState('')
    const [password, setPassword] = useState('')
    const [address, setAddress] = useState('')

    const usernameInput = useRef()
    const emailInput = useRef()
    const phoneNumberInput = useRef()
    const passwordInput = useRef()
    const addressInput = useRef()

    const onClickSignUp = () => {
        SignUpUser(username, email, phonenumber, password, address)
            .then(response => {
                navigation.navigate('Login')
                console.log('Test data store data ', response.data)
            })
            .catch(error => {
                console.log('eror', JSON.stringify(error))
            })
    }

    const _hasErrors = () => {
        return !email.includes('@');
    }
    return (
        <ScrollView contentContainerStyle={{ flexGrow: 1 }}>
            <ImageBackground style={{ flex: 1, backgroundColor: 'white' }}
            >
                <StatusBar translucent={true} backgroundColor={'transparent'} barStyle="dark-content" />
                <SafeAreaView style={{ flex: 1, }} forceInset={{ top: 'always' }}  >
                    <View style={{ flex: 1, flexDirection: 'column', marginRight: '5%', }}  >
                        <View style={styles.flexRow1}  >
                            <View style={{
                                flex: 0.9,
                                flexDirection: 'row',
                                justifyContent: 'flex-start',
                                alignItems: 'flex-start',
                                marginTop: '6%'
                            }}>
                                <TouchableOpacity onPress={() => navigation.goBack()}  >
                                    <Avatar.Icon size={50} color={'black'} icon="chevron-left" style={{ backgroundColor: Colors.white }} />
                                </TouchableOpacity>
                            </View>
                            <View style={{
                                flex: 1.5,
                                flexDirection: 'column',
                                justifyContent: 'flex-start',
                                alignItems: 'flex-start',
                                marginTop: '6%'
                            }}>
                                <Title style={{ fontSize: 22 }} >Edit Profile</Title>
                            </View>
                        </View>
                    </View>
                    <View style={styles.flexRow1}>
                        <Image
                            style={styles.image1}
                            source={{
                                uri: 'https://images.unsplash.com/photo-1582384786360-e3bc0a20a574?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=60',
                            }} />

                    </View>

                    <View style={{ flex: 1, flexDirection: 'column', justifyContent: 'center', alignItems: 'center', margin: '5%' }}  >
                        <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'center', alignItems: 'center', marginLeft: '5%', marginRight: '5%', marginTop: '5%' }}  >
                            <TextInput
                                ref={usernameInput}
                                mode='flat'
                                style={{ width: "70%", backgroundColor: 'transparent', paddingHorizontal: 0 }}
                                label='Username'
                                value={username}
                                autoCapitalize="none"
                                returnKeyType="next"
                                onChangeText={text => setUsername(text)}
                                onSubmitEditing={() => emailInput.current.focus()}
                            />
                        </View>
                        <View style={{ flex: 1, flexDirection: 'column', justifyContent: 'center', alignItems: 'center', marginLeft: '5%', marginRight: '5%', marginTop: '3%' }}  >

                            <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}  >
                                <TextInput
                                    ref={emailInput}
                                    mode='flat'
                                    style={{ width: "70%", backgroundColor: 'transparent', paddingHorizontal: 0 }}
                                    label='Email'
                                    value={email}
                                    autoCapitalize="none"
                                    returnKeyType="next"
                                    onChangeText={text => setEmail(text)}
                                    onSubmitEditing={() => phoneNumberInput.current.focus()}
                                />

                            </View>
                            <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}  >
                                <HelperText
                                    type="error"
                                    visible={_hasErrors()}
                                >
                                    Email address is invalid!
                        </HelperText>
                            </View>


                            <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}  >

                            </View>
                            <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}  >

                            </View>
                        </View>
                        <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'center', alignItems: 'center', marginLeft: '5%', marginRight: '5%', marginTop: '3%' }}  >
                            <TextInput
                                ref={phoneNumberInput}
                                mode='flat'
                                style={{ width: "70%", backgroundColor: 'transparent', paddingHorizontal: 0 }}
                                label='Phone'
                                value={phonenumber}
                                autoCapitalize="none"
                                returnKeyType="next"
                                onChangeText={text => setphonenumber(text)}
                                onSubmitEditing={() => passwordInput.current.focus()}
                            />
                        </View>
                        <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'center', alignItems: 'center', marginLeft: '5%', marginRight: '5%', marginTop: '3%' }}  >
                            <TextInput
                                ref={passwordInput}
                                mode='flat'
                                style={{ width: "70%", backgroundColor: 'transparent', paddingHorizontal: 0 }}
                                label='Password'
                                value={password}
                                secureTextEntry
                                textContentType="password"
                                autoCapitalize="none"
                                returnKeyType="next"
                                onChangeText={text => setPassword(text)}
                                onSubmitEditing={() => addressInput.current.focus()}
                            />
                        </View>
                        <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'center', alignItems: 'center', marginLeft: '5%', marginRight: '5%', marginTop: '3%' }}  >
                            <TextInput
                                ref={addressInput}
                                mode='flat'
                                style={{ width: "70%", backgroundColor: 'transparent', paddingHorizontal: 0 }}
                                label='Address'
                                value={address}
                                autoCapitalize="none"
                                returnKeyType="next"
                                onChangeText={text => setAddress(text)}
                            />
                        </View>
                        <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'center', alignItems: 'center', marginLeft: '5%', marginRight: '5%', marginTop: 34 }}  >
                            <Button style={styles.button} color="#66B5F8" icon="logout-variant" mode="contained" onPress={() => onClickSignUp()}>
                                <Text> Sign Up</Text>
                            </Button>
                        </View>
                    </View>
                </SafeAreaView>
            </ImageBackground>
        </ScrollView>
    )
}

const styles = StyleSheet.create({
    button: {
        zIndex: 0,
        borderRadius: 25,
        width: 135,
        height: 40,
        justifyContent: 'center',
        elevation: 25,
    },
    Title: {
        fontSize: 30,
        color: 'black'
    },
    flexRow1: {
        flex: 0,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginLeft: '5%',
        marginRight: '5%',
        marginTop: '5%'
    },
    image1: {
        width: 150,
        height: 150,
        marginTop: 10,
        borderRadius: 100
    },
});

export default (Register)

