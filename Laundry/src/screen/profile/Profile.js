import React, { useEffect, useState } from 'react';
import { View, StatusBar, SafeAreaView, ScrollView, StyleSheet, Image, TouchableOpacity, Text } from 'react-native';
import { Avatar, Colors, Button, Title, TextInput } from 'react-native-paper';
import { connect } from 'react-redux';
import { GetProfileUser } from '../../api/index';

const Profile = ({ navigation, tokenUser }) => {
    const [dataUser, setDatauser] = useState({})
    const [isloading, setIsloading] = useState(false)
    const [username, setUsername] = useState()
    const [email, setEmail] = useState()
    const [password, setPassword] = useState()
    const [phone, setPhone] = useState()
    const [address, setAddress] = useState()

    useEffect(() => {
        GetProfile()
    }, [])

    const GetProfile = () => {
        setIsloading(true)
        GetProfileUser(tokenUser)
            .then(response => {
                setDatauser(response.data)
                console.log('Test Profile : ', response.data)
                setIsloading(false)
            })
            .catch(error => {
                console.log('eror', JSON.stringify(error))
            })
    }

    return (
        <ScrollView style={{ flex: 1, }} >
            <StatusBar translucent={true} backgroundColor={'transparent'} barStyle="dark-content" />
            <SafeAreaView style={{ flex: 1, }} forceInset={{ top: 'always' }}  >
                <View style={{ flex: 1, flexDirection: 'column', backgroundColor: 'white', }} >
                    <View style={styles.flexRow1}  >
                        <View style={{
                            flex: 0.8,
                            flexDirection: 'row',
                            justifyContent: 'flex-start',
                            alignItems: 'flex-start',
                            marginTop: '6%'
                        }}>
                            <TouchableOpacity onPress={() => navigation.goBack()}  >
                                <Avatar.Icon size={50} color={'black'} icon="chevron-left" style={{ backgroundColor: Colors.white }} />
                            </TouchableOpacity>
                        </View>
                        <View style={{
                            flex: 1.5,
                            flexDirection: 'column',
                            justifyContent: 'flex-start',
                            alignItems: 'flex-start',
                            marginTop: '6%'
                        }}>
                            <Title style={{ fontSize: 22 }} >Profile</Title>
                        </View>
                    </View>
                    <View style={{ flex: 1, flexDirection: 'column', backgroundColor: 'white', alignItems: 'center' }} >
                        <Image
                            style={styles.image1}
                            source={{
                                uri: 'https://images.unsplash.com/photo-1582384786360-e3bc0a20a574?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=60',
                            }} />
                        <View style={{ flex: 1, flexDirection: 'column', alignItems: 'center', width: 320, margin: 15, }}>
                            <Title>Username</Title>
                            <TextInput
                                style={{ width: "80%", backgroundColor: 'transparent', height: '8%' }}
                                value={username}
                                defaultValue={dataUser.Username}
                                onChangeText={text => setUsername(text)}
                            />
                            <Title>Email</Title>
                            <TextInput
                                mode='Flat'
                                style={{ width: "80%", backgroundColor: 'transparent', height: '8%' }}
                                value={email}
                                defaultValue={dataUser.Email}
                                onChangeText={text => setEmail(text)}
                            />
                            <Title>Phone</Title>
                            <TextInput
                                mode='Flat'
                                style={{ width: "80%", backgroundColor: 'transparent', height: '8%' }}
                                value={phone}
                                defaultValue={dataUser.Phone}
                                onChangeText={text => setPhone(text)}
                            />
                            <Title>Address</Title>
                            <TextInput
                                mode='Flat'
                                style={{ width: "80%", backgroundColor: 'transparent', height: '8%' }}
                                value={address}
                                defaultValue={dataUser.Address}
                                onChangeText={text => setAddress(text)}

                            />
                            <Title style={{ color: '#CBCBCB' }}   >password</Title>
                            <TextInput
                                mode='Flat'
                                style={{ width: "80%", backgroundColor: 'transparent', height: '8%' }}
                                defaultValue={'111111'}
                                secureTextEntry
                                disabled
                            />
                            {/* <View style={{ padding: '10%' }}>
                                <Button style={styles.button} color="#66B5F8" mode="contained" onPress={() => Login()}  >
                                    <Text> Save</Text>
                                </Button>
                            </View> */}
                        </View>
                    </View>
                </View>
            </SafeAreaView>
        </ScrollView>
    )
}

const styles = StyleSheet.create({
    image: {
        height: 130,
        width: 130,
        borderRadius: 40,
    },
    image1: {
        width: 150,
        height: 150,
        marginTop: 10,
        borderRadius:100
    },
    button: {
        zIndex: 0,
        borderRadius: 25,
        width: 135,
        height: 40,
        justifyContent: 'center',
        elevation: 25,
    },
    flexRow1: {
        flex: 0,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginLeft: '5%',
        marginRight: '5%',
        marginTop: '5%'
    },

});

const mapStateToProps = state => {
    return {
        tokenUser: state.login.token,
    }
}

export default connect(mapStateToProps, null)(Profile)