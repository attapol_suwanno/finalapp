import React, { useState, useEffect } from 'react';
import { View, Text, ImageBackground, StatusBar, SafeAreaView, ScrollView, StyleSheet, Image, TouchableOpacity, FlatList } from 'react-native';
import {  Title, Avatar, List, Colors, Divider } from 'react-native-paper';
import { connect } from 'react-redux';
import { ShowhistoryUserOrder } from '../../api/index'

const History = ({ navigation, tokenUser }) => {
    const [isloading, setIsloading] = useState(false)
    const [datahistory, setdatahistory] = useState([])

    useEffect(() => {
        gethistory()
    }, [])

    const workingRefresh = () => {
        setIsloading(true)
        gethistory()
        setTimeout(() => {
            setIsloading(false)
        }, 2000)
    }
    const gethistory = () => {
        ShowhistoryUserOrder(tokenUser)
            .then(response => {
                setdatahistory(response.data)
                console.log('Test History : ', response.data)
            })
            .catch(error => {
                console.log('eror', JSON.stringify(error))
            })
    }

    const myRendernumber = (item) => {
        return (
            <TouchableOpacity style={styles.border}
            >
                <Divider />
                <List.Section >
                    <List.Item
                        left={() => (
                            <Image
                                style={{ width: 50, height: 50, marginRight: 10, borderRadius: 8 }}
                                source={{
                                    uri: item.Filename ,
                                }} />
                        )}
                        title={'' + item.Storename}
                        description={'Status :' + (item.Status === 1 ? "Pending" :item.Status ===2 ? "processing" :"Finished")}
                    />
                </List.Section>
            </TouchableOpacity>
        )
    }

    return (
        <ScrollView contentContainerStyle={{ flexGrow: 1 }}>
            <ImageBackground style={{ flex: 1, backgroundColor: 'white' }}
            >
                <SafeAreaView style={{ flex: 1, }} forceInset={{ top: 'always' }}  >
                    <StatusBar translucent={true} backgroundColor={'transparent'} barStyle="dark-content" />
                    <View style={styles.flexRow1} >
                        <View style={styles.flexRow3}  >
                            <View style={{
                                flex: 1,
                                flexDirection: 'row',
                                justifyContent: 'flex-start',
                                alignItems: 'flex-start',
                                marginTop: '6%'
                            }}>
                                <TouchableOpacity onPress={() => navigation.goBack()}  >
                                    <Avatar.Icon size={50} color={'black'} icon="chevron-left" style={{ backgroundColor: Colors.white }} />
                                </TouchableOpacity>
                            </View>
                            <View style={{
                                flex: 1.5,
                                flexDirection: 'column',
                                justifyContent: 'flex-start',
                                alignItems: 'flex-start',
                                marginTop: '6%'
                            }}>
                                <Title style={{ fontSize: 25 }} >History</Title>
                                <View style={{ backgroundColor: 'gold', borderRadius: 20, padding: '2%', }}>
                                    <Text>{datahistory.length}{'  List'}</Text>
                                </View>
                            </View>
                        </View>
                        <View style={styles.flexRow5}  >
                            <FlatList
                                data={datahistory}
                                renderItem={({ item }) => myRendernumber(item)}
                                keyExtractor={(item, index) => index + ''}
                                refreshing={isloading}
                                onRefresh={workingRefresh}
                            >
                            </FlatList>
                        </View>
                    </View>
                </SafeAreaView>
            </ImageBackground>
        </ScrollView>
    )
}

const styles = StyleSheet.create({
    flexRow1: {
        flex: 1,
    },
    flexRow3: {
        flex: 0,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginLeft: '5%',
        marginRight: '5%',
        marginTop: '5%'
    },
    flexRow5: {
        flex: 1,
        flexDirection: 'column',
    },
    border: {
        borderColor: '#FDFEFE',
        marginHorizontal: '5%',
    },
});

const mapStateToProps = state => {
    return {
        tokenUser: state.login.token,
    }
}

export default connect(mapStateToProps, null)(History)

