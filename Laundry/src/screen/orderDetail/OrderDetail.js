import React, { useState, useEffect } from 'react';
import { View, ImageBackground, StatusBar, SafeAreaView, ScrollView, StyleSheet, TouchableOpacity, Text, Image, Linking } from 'react-native';
import { Avatar, Title, Button, Divider } from 'react-native-paper';
import { connect } from 'react-redux';
import { GetListDetailOrderUser, deleteOrder, host } from '../../api/index'


const OrderDetail = ({ route, navigation, tokenUser }) => {
    const [valueNormal, setValueNormal] = useState({})
    const [Service, setServer] = useState(0)

    const TotalPrice = valueNormal.Price + valueNormal.Dry + valueNormal.Detergent + valueNormal.Fabricsof + Service

    useEffect(() => {
        console.log('test param :', route.params.idOrder)
        Getdetail(route.params.idOrder)
    }, [])

    const Getdetail = (id) => {
        GetListDetailOrderUser(id, tokenUser)
            .then(response => {
                setValueNormal(response.data)
                console.log('Test orderdetail : ', response.data)
            })
            .catch(error => {
                console.log('eror', JSON.stringify(error))
            })
    }

    const onclickDelete = () => {
        console.log('test id :', valueNormal.Ordersid)
        var idDelete = valueNormal.Ordersid
        deleteOrder(idDelete, tokenUser)
            .then(response => {
                navigation.navigate('OrderListUser')
                console.log('delete order : ', response.data)
            })
            .catch(error => {
                console.log('eror', JSON.stringify(error))
            })
    }

    return (
        <ScrollView contentContainerStyle={{ flexGrow: 1 }}>
            <StatusBar translucent={true} backgroundColor={'transparent'} barStyle="dark-content" />
            <ImageBackground style={styles.backgroundMain}
                source={{ uri: valueNormal.Filename }}
            >
                <SafeAreaView style={styles.sefeArea} forceInset={{ top: 'always' }}  >
                    <View style={{ flex: 1, flexDirection: 'column', marginLeft: '5%' }}  >
                        <TouchableOpacity onPress={() => navigation.goBack()}  >
                            <Avatar.Icon size={60} icon="chevron-left" style={{ backgroundColor: 'transparent', marginTop: 14 }} />
                        </TouchableOpacity>
                        <View style={styles.locationflex}>
                            <View>
                                <Title style={styles.nameStore} textStyle={{ flexWrap: 'wrap' }} >{valueNormal.Storename}</Title>
                            </View>
                            <View style={styles.locationIcon}>
                                <View>
                                    <Avatar.Icon size={70} icon="map-marker" style={{ backgroundColor: 'transparent', marginTop: 0 }} />
                                </View>
                                <View>
                                    <Title style={styles.locationText}>Location</Title>
                                    <Title style={styles.TextKm}>0.23KM.</Title>
                                </View>
                            </View>
                        </View>
                    </View>
                </SafeAreaView>
            </ImageBackground>
            <View style={styles.flex5}>
                <Title style={styles.TextTitle}>Order information</Title>
                <View style={styles.flex8}>
                    <View style={styles.flex9}  >
                        <Text style={styles.Text}>Wash</Text>
                        <Text style={styles.Text}>Dry</Text>
                        <Text style={styles.Text}>Laundry  Detergent</Text>
                        <Text style={styles.Text}>Fabric Softener</Text>
                    </View>
                    <View style={styles.flex10}  >
                        <Text style={styles.Text}>THB{valueNormal.Price}</Text>
                        <Text style={styles.Text}>THB{valueNormal.Dry}</Text>
                        <Text style={styles.Text}>THB{valueNormal.Detergent}</Text>
                        <Text style={styles.Text}>THB{valueNormal.Fabricsof}</Text>
                    </View>
                </View>
                <Divider style={styles.Text} />
                <View style={styles.flex8}>
                    <View style={styles.flex9}  >
                        <Text style={styles.Text}>Service</Text>
                        <Text style={styles.Text}>Total</Text>
                    </View>
                    <View style={styles.flex10}  >
                        <Text style={styles.Text}>THB{Service}</Text>
                        <Text style={styles.Text}>THB{TotalPrice}</Text>
                    </View>
                </View>
                <View style={styles.flex11}  >
                    <Button style={styles.button} color="#FD2D2D" mode="contained" onPress={() => onclickDelete()} >
                        <Text> cancel</Text>
                    </Button>
                </View>
            </View>
        </ScrollView>
    )
}

const styles = StyleSheet.create({
    backgroundMain: {
        width: 360,
        height: 235
    },
    sefeArea: {
        flex: 1,
        flexDirection: 'column'
    },
    flex4: {
        flexDirection: 'column',
    },
    flex5: {
        flexDirection: 'column',
        backgroundColor: "white",
        paddingHorizontal: 32
    },
    flex6: {
        flexDirection: 'row',
        marginTop: 16
    },
    flex7: {
        flexDirection: 'column',
        backgroundColor: "white",
    },
    flex8: {
        flexDirection: 'row'
    },
    flex9: {
        flex: 1,
    },
    flex10: {
        flex: 0,
        flexDirection: 'column',
        alignItems: 'flex-end',
    },
    flex11: {
        flex: 0,
        alignItems: 'center',
        marginTop: 32,
        marginBottom: 15

    },
    button: {
        zIndex: 0,
        borderRadius: 25,
        width: 135,
        height: 30,
        justifyContent: 'center',
        elevation: 25,
    },
    absolute: {
        position: "absolute",
        top: 0,
        left: 0,
        bottom: 0,
        right: 0
    },
    TitleName: {
        borderWidth: 1,
        backgroundColor: 'white',
        borderColor: 'white',
        padding: '1.5%',
        borderRadius: 15
    },
    Text: {
        marginTop: 16
    },
    TextTitle: {
        marginTop: 32
    },
    nameStore: {
        marginTop: 50,
        color: 'white',
        fontSize: 24
    },
    locationflex: {
        flexDirection: 'column',
        width: 400,
        marginLeft: 16
    },
    locationIcon: {
        flexDirection: 'row',
    },
    locationText: {
        fontSize: 14,
        color: 'white',
        padding: 0
    },
    TextKm: {
        color: 'white',
        marginTop: 0,
        fontSize: 22,
    }
})

const mapStateToProps = state => {
    return {
        tokenUser: state.login.token,
    }
}


export default connect(mapStateToProps, null)(OrderDetail)
