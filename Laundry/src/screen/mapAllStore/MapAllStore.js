import React, { useState, useEffect, useRef } from 'react';
import MapView, { PROVIDER_GOOGLE, Marker, Callout } from 'react-native-maps'; // remove PROVIDER_GOOGLE import if not using Google Maps
import { View, Text, StatusBar, SafeAreaView, ScrollView, StyleSheet, Image, Dimensions, TouchableOpacity } from 'react-native';
import Geolocation from 'react-native-geolocation-service';
import { Colors, IconButton, Avatar } from 'react-native-paper';
import { check, request, PERMISSIONS, RESULTS, openSettings } from 'react-native-permissions'
import Carousel   from 'react-native-snap-carousel';
import { connect } from 'react-redux';
import { ListStoreAll } from '../../api/index'

const MapAllStore = () => {
    const [thisLocation, setThisLocation] = useState({
        latitude: 18.796470,
        longitude: 98.953280,
    })
    const [allLocation, setAllLocation] = useState([])
    const [storeDataAll, setStoreDataAll] = useState([])
    const MapViewRef = useRef()

    useEffect(() => {
        StoreAll()
    }, [])

    const StoreAll = () => {
        ListStoreAll()
            .then(response => {
                setStoreDataAll(response.data)
                setAllLocation(response.data)
                console.log('Test data Location ', response.data)
            })
            .catch(error => {
                console.log('eror', JSON.stringify(error))
            })
    }

    useEffect(() => {
        setAllLocation(storeDataAll)
        check(PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION).then(result => {
            if (result === RESULTS.GRANTED) {
                callLocation((position) => {
                    setThisLocation(position.coords)
                })
                return
            }
            if (result === RESULTS.DENIED) {
                request(PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION).then(result => {
                    if (result === RESULTS.GRANTED) {
                        callLocation((position) => {
                            setThisLocation(position.coords)
                        })
                    }
                    else if (result === RESULTS.DENIED) {
                        alert('Please Allow to Access this device Location')
                    }
                    else if (result === RESULTS.BLOCKED) {
                        alert('You can Allow again to Use Any feature')
                    }
                })
            }
        })
    }, [])

    const callLocation = (callback) => {
        Geolocation.getCurrentPosition(
            (position) => {
                callback(position)
            },
            (error) => {
                console.log(error.code, error.message);
            },
            { enableHighAccuracy: true, timeout: 15000, maximumAge: 10000 }
        )
    }

    const renderCarouselItem = ({ item }) =>
        < View style={styles.cardContainer} >
            <View style={{ flex: 1, flexDirection: 'column', justifyContent: 'flex-start', alignItems: 'flex-start' }}  >
                <Image style={styles.cardImage} source={{ uri: item.Filename }} />
            </View>
            <View style={{ flex: 1, flexDirection: 'column', justifyContent: 'center', alignItems: 'center' }}  >
                <View style={{ flex: 1, flexDirection: 'column', justifyContent: 'center', alignItems: 'center' }}  >
                    <Text style={styles.cardTitle}>{item.Storename}</Text>
                </View>
                <View style={{ flex: 2, flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}  >
                    <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}  >
                        <IconButton
                            icon="washing-machine"
                            color={Colors.black}
                            size={25}
                        />
                        <Text>{item.Prices[0].Pricesmall} ขึ้นไป</Text>
                    </View>
                    <View style={{ flex: 2, flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}  >
                        <IconButton
                            icon="navigation"
                            color={Colors.black}
                            size={25}
                        />
                        <Text>2 km</Text>
                    </View>
                </View>
            </View>
        </View >


    const onCarouselItemChange = (index) => {
        MapViewRef.current.animateToRegion({
            latitude: allLocation[index].Location[0].Latitude,
            longitude: allLocation[index].Location[0].Longitude,
            latitudeDelta: 0.09,
            longitudeDelta: 0.035
        })
    }

    return (
        <ScrollView contentContainerStyle={{ flexGrow: 1 }}>
            <StatusBar translucent={true} backgroundColor={'transparent'} barStyle="dark-content" />
            <SafeAreaView style={{ flex: 1, }} forceInset={{ top: 'always' }}  >
                <View style={{ flex: 3, }}  >
                    <View style={styles.container}>
                        <MapView
                            ref={MapViewRef}
                            provider={PROVIDER_GOOGLE} // remove if not using Google Maps
                            style={styles.map}
                            region={{
                                latitude: thisLocation.latitude,
                                longitude: thisLocation.longitude,
                                latitudeDelta: 0.015,
                                longitudeDelta: 0.0121,
                            }}
                        >
                            {
                                !allLocation.Location === null ?
                                    null
                                    :
                                    allLocation.map((item, index) => {
                                        return (
                                            <Marker draggable
                                                coordinate={{ latitude: item.Location[0].Latitude, longitude: item.Location[0].Longitude }}
                                                title={item.Storename}
                                                centerOffset={{ x: -18, y: -60 }}
                                                anchor={{ x: 0.69, y: 1 }}
                                            >
                                                <Image source={{ uri: item.Filename }} style={{ height: 35, width: 35, borderRadius: 25, }} />
                                                <Callout>
                                                    <Text>{item.Storename}</Text>
                                                </Callout>
                                            </Marker>
                                        )
                                    }
                                    )
                            }
                        </MapView>
                        <Carousel
                            data={allLocation}
                            containerCustomStyle={styles.carousel}
                            renderItem={renderCarouselItem}
                            sliderWidth={Dimensions.get('window').width}
                            itemWidth={300}
                            removeClippedSubviews={false}
                            onSnapToItem={(index) => onCarouselItemChange(index)}

                        />
                    </View>
                </View>
            </SafeAreaView>
        </ScrollView>
    )
}


const styles = StyleSheet.create({
    container: {
        ...StyleSheet.absoluteFillObject,
        height: '100%',
        width: '100%',
        justifyContent: 'flex-end',
        alignItems: 'center',
    },
    map: {
        ...StyleSheet.absoluteFillObject,
    },
    carousel: {
        position: 'absolute',
        bottom: 0,
        marginBottom: 48
    },
    cardContainer: {
        flexDirection: 'row',
        backgroundColor: Colors.white,
        height: 120,
        width: 300,

        borderRadius: 24
    },
    cardImage: {
        height: 120,
        width: 130,
        position: 'absolute',
        borderRadius: 24,
    },
    cardTitle: {
        color: 'black',
        fontSize: 17,
        alignSelf: 'center'
    }

});



export default (MapAllStore)
