import React, { useState, useEffect } from 'react';
import { View, ImageBackground, StatusBar, SafeAreaView, ScrollView, StyleSheet, TouchableOpacity, Text } from 'react-native';
import { Avatar, Paragraph, Title, Colors, Button, RadioButton, Checkbox, } from 'react-native-paper';
import { connect } from 'react-redux';
import { getStorebyId, CreateOrder, host } from '../../api/index'


const StoreDetail = ({ navigation, tokenUser, route }) => {
    const [dataStore, setDataStore] = useState([])
    const [checked, setchecked] = useState(0)
    const [checkDry, setCheckDry] = useState(false)
    const [checkDryValue, setCheckDryValue] = useState(0)
    const [checkdetergent, setCheckDetergent] = useState(false)
    const [checkdetergentValue, setCheckDetergentValue] = useState(0)
    const [checkFabrisoftener, setcheckFabrisoftener] = useState(false)
    const [checkFabrisoftenerValue, setcheckFabrisoftenerValue] = useState(0)

    useEffect(() => {
        GetDatabyid(route.params.sentId)
    }, [])
    const GetDatabyid = (id) => {
        getStorebyId(id)
            .then(response => {
                setDataStore(response.data)
                console.log('Test data store By id ', response.data)
            })
            .catch(error => {
                console.log('eror', JSON.stringify(error))
            })
    }

    const CreateOrderCustomer = () => {
        {
            tokenUser != "" ?
                CreateOrder(dataStore.ID, checked, checkDryValue, checkdetergentValue, checkFabrisoftenerValue, tokenUser)
                    .then(response => {
                        console.log('data order customer', response.data)
                        navigation.navigate('OrderListUser')
                    })
                    .catch(error => {
                        console.log('eror', JSON.stringify(error))
                    })
                :
                navigation.navigate('Login')
        }

    }

    return (
        dataStore === null ?
            null
            :
            <ScrollView contentContainerStyle={{ flexGrow: 1 }}>
                <StatusBar translucent={true} backgroundColor={'transparent'} barStyle="dark-content" />
                <ImageBackground style={styles.backgroundMain}
                    source={{ uri: dataStore.Filename != null ? dataStore.Filename : "https://f.ptcdn.info/077/060/000/pg22y32nzpJerZ6Zocwl-o.jpg" }}
                >
                    <SafeAreaView style={styles.sefeArea} forceInset={{ top: 'always' }}  >
                        <View style={{ flex: 1, flexDirection: 'column', }}  >
                            <TouchableOpacity onPress={() => navigation.goBack()}  >
                                <Avatar.Icon size={60} icon="chevron-left" style={{ backgroundColor: 'transparent', marginTop: 14 }} />
                            </TouchableOpacity>
                            <View style={styles.locationflex}>
                                <View>
                                    <Title style={styles.nameStore} textStyle={{ flexWrap: 'wrap' }} >{dataStore.Storename}</Title>
                                </View>
                                <View style={styles.locationIcon}>
                                    <View>
                                        <Avatar.Icon size={70} icon="map-marker" style={{ backgroundColor: 'transparent', marginTop: 0 }} />
                                    </View>
                                    <View>
                                        <Title style={styles.locationText}>Location</Title>
                                        <Title style={styles.TextKm}>0.23KM.</Title>
                                    </View>
                                </View>
                            </View>
                        </View>
                        <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'flex-end', marginRight: '5%', marginTop: '5%' }}  >
                            {
                                !dataStore.Location ?
                                    null
                                    :
                                    dataStore.Location.map((item) => {

                                        return (
                                            <TouchableOpacity onPress={() => navigation.navigate('MapStore', { latitudeStore: item.Latitude, longtitudeStore: item.Longitude, datastore: dataStore })}  >
                                                <Avatar.Icon size={50} icon="map" style={{ backgroundColor: Colors.white }} />
                                            </TouchableOpacity>
                                        )

                                    })}

                        </View>
                    </SafeAreaView>
                </ImageBackground>
                <View style={styles.flex5}>
                    <View style={styles.flex8}  >
                        <Title style={styles.nameTitle}>Wash option</Title>
                        {
                            !dataStore.prices ?
                                null
                                :
                                dataStore.prices.map((item) => {
                                    return (
                                        <View style={styles.flex7}>
                                            <RadioButton.Group

                                                onValueChange={value => setchecked(value)}
                                                value={checked}
                                            >
                                                <View style={styles.flex81}>
                                                    <RadioButton
                                                        color={'#66B5F8'}
                                                        value={item.Pricesmall}
                                                    />
                                                    <Title style={styles.titlePrice}>{item.Pricesmall}</Title>
                                                    <Paragraph>{'THB.'}</Paragraph>
                                                </View>
                                                <View style={styles.flex81}>
                                                    <RadioButton
                                                        color={'#66B5F8'}
                                                        value={item.PriceMiddle}
                                                    />
                                                    <Title style={styles.titlePrice}>{item.PriceMiddle}</Title>
                                                    <Paragraph>{'THB.'}</Paragraph>
                                                </View>
                                                <View style={styles.flex81}>
                                                    <RadioButton
                                                        color={'#66B5F8'}
                                                        value={item.PriceBig}
                                                    />
                                                    <Title style={styles.titlePrice}>{item.PriceBig}</Title>
                                                    <Paragraph>{'THB.'}</Paragraph>
                                                </View>
                                            </RadioButton.Group>
                                        </View>
                                    )

                                })
                        }
                    </View>
                    <View style={styles.flex8}  >
                        <Title style={styles.nameTitle}>More Option</Title>
                        <View style={styles.flex7}  >
                            <Checkbox
                                color={'#66B5F8'}
                                status={checkDry ? 'checked' : 'unchecked'}
                                onPress={() => {
                                    setCheckDry(!checkDry)
                                    if (checkDry === true) {
                                        setCheckDryValue(0)
                                    } else {
                                        setCheckDryValue(dataStore.Dry)
                                    }
                                    ;
                                }}
                            />
                            <Title>{'Dry Option                      ' + dataStore.Dry + 'THB.'}</Title>
                        </View>
                    </View>
                    <View style={styles.flex8}  >
                        <View style={styles.flex7}  >
                            <Checkbox
                                color={'#66B5F8'}
                                status={checkdetergent ? 'checked' : 'unchecked'}
                                onPress={() => {
                                    setCheckDetergent(!checkdetergent)
                                    if (checkdetergent === true) {
                                        setCheckDetergentValue(0)
                                    } else {
                                        setCheckDetergentValue(dataStore.Detergent)
                                    }
                                }}
                            />
                            <Title>{'Laundry  Detergent        ' + dataStore.Detergent + 'THB.'}</Title>
                        </View>
                    </View>
                    <View style={styles.flex8}  >
                        <View style={styles.flex7}  >
                            <Checkbox
                                color={'#66B5F8'}
                                value={dataStore.Fabricsof}
                                status={checkFabrisoftener ? 'checked' : 'unchecked'}
                                onPress={() => {
                                    setcheckFabrisoftener(!checkFabrisoftener);
                                    if (checkFabrisoftener === true) {
                                        setcheckFabrisoftenerValue(0)
                                    } else {
                                        setcheckFabrisoftenerValue(dataStore.Fabricsof)
                                    }
                                }}
                            />
                            <Title>{'Fabric Softener              ' + dataStore.Fabricsof + 'THB.'}</Title>
                        </View>
                    </View>
                    <View style={styles.flex9}  >
                        <Button style={styles.button} color="#66B5F8" mode="contained" onPress={() => CreateOrderCustomer()} >
                            <Text style={styles.text} >submit</Text>
                        </Button>
                    </View>
                </View>
            </ScrollView >
    )
}

const styles = StyleSheet.create({
    backgroundMain: {
        width: 360,
        height: 235
    },
    sefeArea: {
        flex: 1,
        flexDirection: 'row'
    },
    flex5: {
        flex: 0,
        flexDirection: 'column',
        padding: 18
    },
    flex6: {
        flex: 1,
        flexDirection: 'column',
    },
    flex7: {
        flex: 1,
        flexDirection: 'row',
        marginTop: 19
    },
    flex8: {
        flex: 1,
        flexDirection: 'column',
    },
    flex81: {
        flex: 1,
        flexDirection: 'column',
        alignItems: 'center',
        borderWidth: 1,
        margin: 8,
        borderRadius: 8,
        borderColor: '#66B5F8'
    },
    flex9: {
        flex: 0,
        flexDirection: 'column',
        alignItems: 'center',
        marginTop: 34
    },
    button: {
        zIndex: 0,
        borderRadius: 25,
        width: 135,
        height: 30,
        justifyContent: 'center',
        elevation: 25,
        height: 45
    },
    absolute: {
        position: "absolute",
        top: 0,
        left: 0,
        bottom: 0,
        right: 0
    },
    TitleName: {
        borderWidth: 1,
        backgroundColor: 'white',
        borderColor: 'white',
        padding: '1.5%',
        borderRadius: 15
    },
    titlePrice: {
        fontSize: 24
    },
    nameTitle: {
        marginTop: 16
    },
    text: {
        color: 'white'
    },
    nameStore: {
        marginTop: 50,
        color: 'white',
        fontSize: 24
    },
    locationflex: {
        flexDirection: 'column',
        width: 400,
        marginLeft: 16
    },
    locationIcon: {
        flexDirection: 'row',
    },
    locationText: {
        fontSize: 14,
        color: 'white',
        padding: 0
    },
    TextKm: {
        color: 'white',
        marginTop: 0,
        fontSize: 22,
    }
})

const mapStateToProps = state => {
    return {
        tokenUser: state.login.token,
    }
}
export default connect(mapStateToProps, null)(StoreDetail)
