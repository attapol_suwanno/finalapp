import React, { useState } from 'react';
import { View, Text, ImageBackground, StatusBar, SafeAreaView, ScrollView, StyleSheet, Image, TouchableOpacity, Alert } from 'react-native';
import { Button, TextInput, Title, Paragraph, } from 'react-native-paper';
import { connect } from 'react-redux';
import { addTokenId } from '../../action/LoginAction';
import { loginUser } from '../../api/index'

const Login = ({ navigation, addTokenId }) => {
    const [username, setUsername] = useState('')
    const [password, setPassword] = useState('')

    const Login = () => {
        loginUser(username, password)
            .then(response => {
                addTokenId(response.data.token)
                console.log('Test login :', response.data.token)
                navigation.navigate('mainRouter')
            })
            .catch(error => {
                console.log('eror', JSON.stringify(error))
                Alert.alert("Invalid", "Email or Password Incorrect")
            })
    }

    return (
        <ScrollView contentContainerStyle={{ flexGrow: 1 }}>
            <ImageBackground style={{ flex: 1, backgroundColor: 'white' }}
            >
                <SafeAreaView style={{ flex: 1, }} forceInset={{ top: 'always' }}  >
                    <StatusBar translucent={true} backgroundColor={'transparent'} barStyle="white-content" />
                    <View style={styles.flexRow1} >
                        <View style={styles.flexRow2}  >
                            <View style={styles.flexRow3}  >
                                <Title>Welcome to</Title>
                                <Paragraph>Log in to get in the moments update on the things that interest you.</Paragraph>
                            </View>
                            <View style={styles.flexRow4}  >
                                <Image
                                    style={styles.image1}
                                    source={{
                                        uri: 'https://images.unsplash.com/photo-1467043153537-a4fba2cd39ef?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=722&q=80',
                                    }} />
                            </View>
                        </View>
                        <View style={styles.flexRow5}  >
                            <View style={styles.flexRow6}  >
                                <TextInput
                                    mode='text'
                                    style={{ width: "80%", backgroundColor: 'transparent' }}
                                    label='Username'
                                    placeholder="test@hotmail.com"
                                    value={username}
                                    onChangeText={text => setUsername(text)}
                                />
                            </View>
                            <View style={styles.flexRow7}  >
                                <TextInput
                                    mode='text'
                                    style={{ width: "80%", backgroundColor: 'transparent' }}
                                    label='Password'
                                    secureTextEntry
                                    placeholder="*******"
                                    returnKeyType="done"
                                    autoCapitalize="none"
                                    value={password}
                                    onChangeText={text => setPassword(text)}
                                />
                            </View>
                            <View style={styles.flexRow8}  >
                                <Button style={styles.button} color="#66B5F8" icon="logout-variant" mode="contained" onPress={Login}  >
                                    <Text> Sign Up</Text>
                                </Button>
                            </View>
                            <View style={styles.flexRow9}  >
                                <Text>Don't have an account? </Text>
                                <TouchableOpacity mode="contained" onPress={() => navigation.navigate('Register')} >
                                    <Text style={styles.SignUp} > Sign Up Now</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                </SafeAreaView>
            </ImageBackground>
        </ScrollView>
    )
}

const styles = StyleSheet.create({
    button: {
        zIndex: 0,
        borderRadius: 25,
        width: 135,
        height: 40,
        justifyContent: 'center',
        elevation: 25,
    },
    SignUp: {
        color: '#66B5F8'
    },
    flexRow1: {
        flex: 1,
    },
    flexRow2: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginLeft: '5%',
        marginRight: '5%',
        marginTop: '10%',
    },
    flexRow3: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        marginLeft: '5%',
        marginRight: '5%',
    },
    flexRow4: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        marginLeft: '5%',
        marginRight: '5%',
    },
    image1: {
        width: 150,
        height: 200,

    },
    flexRow5: {
        flex: 2,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
    },
    flexRow6: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginLeft: '5%',
        marginRight: '5%',
    },
    flexRow7: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginLeft: '5%',
        marginRight: '5%',
    },
    flexRow8: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginLeft: '5%',
        marginRight: '5%',
        marginTop: '5%'
    },
    flexRow9: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginLeft: '5%',
        marginRight: '5%',
        marginTop: '5%'
    },
});


const mapDispatchToProps = {
    addTokenId
}

export default connect(null, mapDispatchToProps)(Login)

