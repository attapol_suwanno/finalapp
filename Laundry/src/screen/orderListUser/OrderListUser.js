import React, { useEffect, useState } from 'react';
import { View, StatusBar, SafeAreaView, StyleSheet, Image, TouchableOpacity, FlatList, Text } from 'react-native';
import { Avatar, List, Title, Divider } from 'react-native-paper';
import { useFocusEffect } from '@react-navigation/native';
import { connect } from 'react-redux';
import { ListOrederUser } from '../../api/index'

const OrderListUser = ({ navigation, tokenUser }) => {
    const [isloading, setIsloading] = useState(false)
    const [datalist, setDatalist] = useState([])

    useEffect(() => {
        Listorder()
    }, [])

    useFocusEffect(
        React.useCallback(() => {
            Listorder()
        }, [])
    );

    const Listorder = () => {
        setIsloading(true)
        ListOrederUser(tokenUser)
            .then(response => {
                setDatalist(response.data)
                console.log('Test order : ', response.data)
                setIsloading(false)
            })
            .catch(error => {
                console.log('eror', JSON.stringify(error))
            })
    }

    const myRendernumber = ({ item, index }) => {
        return (
            <TouchableOpacity style={styles.border}
                onPress={() => navigation.navigate('OrderDetail', { idOrder: item.Ordersid })}
            >
                <Divider />
                <List.Section >
                    <List.Item
                        left={() => (
                            <Image
                                style={{ width: 50, height: 50, marginRight: 10, borderRadius: 8 }}
                                source={{
                                    uri: item.Filename,
                                }} />
                        )}
                        title={'' + item.Storename}

                        description={'Status :' + (item.Status === 1 ? "Pending" :item.Status ===2 ? "processing" :"Finished")}
                    />
                </List.Section>
            </TouchableOpacity>
        )
    }

    const workingRefresh = () => {
        setIsloading(true)
        Listorder()
        setTimeout(() => {
            setIsloading(false)
        }, 2000)
    }

    return (
        <View style={{ flex: 1, }} >
            <StatusBar translucent={true} backgroundColor={'transparent'} barStyle="dark-content" />
            <SafeAreaView style={{ flex: 1, }} forceInset={{ top: 'always' }}  >
                <View style={{ flex: 1, flexDirection: 'column', }}  >
                    <View style={styles.flexRow3}  >
                        <View style={{
                            flex: 1,
                            flexDirection: 'row',
                            justifyContent: 'flex-start',
                            alignItems: 'flex-start',
                            marginTop: '6%'
                        }}>
                            <TouchableOpacity onPress={() => navigation.goBack()}  >
                                <Avatar.Icon size={50} color={'black'} icon="chevron-left" style={{ backgroundColor: 'transparent', }} />
                            </TouchableOpacity>
                        </View>
                        <View style={{
                            flex: 1.5,
                            flexDirection: 'column',
                            justifyContent: 'flex-start',
                            alignItems: 'flex-start',
                            marginTop: '6%'
                        }}>
                            <Title style={{ fontSize: 25 }} >Order</Title>
                            <View style={{ backgroundColor: 'gold', borderRadius: 20, padding: '2%', }}>
                                <Text>{datalist.length}{'  List'}</Text>
                            </View>
                        </View>
                    </View>
                    {
                        datalist != [] ?
                            <FlatList
                                data={datalist}
                                renderItem={(item) => myRendernumber(item)}
                                keyExtractor={(item, index) => index}
                                refreshing={isloading}
                                onRefresh={workingRefresh}
                            >
                            </FlatList>
                            :
                            null
                    }

                </View>
            </SafeAreaView>
        </View>
    )
}

const styles = StyleSheet.create({
    stretch: {
        width: '55%',
        height: '60%',
        borderRadius: 100,
        marginTop: '15%',
        justifyContent: 'center',
        marginLeft: '20%'
    },
    image: {
        height: 60,
        width: 60,
        margin: 8,
        borderRadius: 40,
    },
    row: {
        flexDirection: 'row',
    },
    column: {
        flexDirection: 'column',
    },
    border: {
        borderColor: '#FDFEFE',
        marginHorizontal: '5%',
    },
    rightItem: {
        backgroundColor: 'red'
    },
    IcontUser: {
        margin: '4%'
    },
    flexRow3: {
        flex: 0,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginLeft: '5%',
        marginRight: '5%',
        marginTop: '5%'
    },
});

const mapStateToProps = state => {
    return {
        tokenUser: state.login.token,
    }
}
export default connect(mapStateToProps, null)(OrderListUser)