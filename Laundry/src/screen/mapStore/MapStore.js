import React, { useState, useEffect, useRef } from 'react';
import MapView, { PROVIDER_GOOGLE, Marker, Callout } from 'react-native-maps'; // remove PROVIDER_GOOGLE import if not using Google Maps
import { View, StatusBar, SafeAreaView, ScrollView, StyleSheet, Image, Text } from 'react-native';
const MapStore = ({ route }) => {
    const [imagePro, setImagePro] = useState('')
    const MapViewRef = useRef()

    useEffect(() => {
        MapViewRef.current.animateCamera({
            center: {
                latitude: route.params.latitudeStore,
                longitude: route.params.longtitudeStore,
            }
        })
    }, [])

    return (
        <ScrollView contentContainerStyle={{ flexGrow: 1 }}>

            <StatusBar translucent={true} backgroundColor={'transparent'} barStyle="dark-content" />
            <SafeAreaView style={{ flex: 1, }} forceInset={{ top: 'always' }}  >
                <View style={{ flex: 3, }}  >
                    <View style={styles.container}>
                        <MapView
                            ref={MapViewRef}
                            provider={PROVIDER_GOOGLE} // remove if not using Google Maps
                            style={styles.map}
                            region={{
                                latitude: route.params.latitudeStore,
                                longitude: route.params.longtitudeStore,
                                latitudeDelta: 0.015,
                                longitudeDelta: 0.0121,
                            }}
                        >
                            <Marker
                                coordinate={{
                                    latitude: route.params.latitudeStore,
                                    longitude: route.params.longtitudeStore,
                                }}
                                centerOffset={{ x: -18, y: -60 }}
                                anchor={{ x: 0.69, y: 1 }}
                            >
                                <Image source={{ uri:route.params.datastore.Filename }} style={{ height: 35, width: 35, borderRadius: 25, }} />
                                <Callout>
                                    <Text>{route.params.datastore.Storename}</Text>
                                </Callout>
                            </Marker>
                        </MapView>
                    </View>
                </View>
            </SafeAreaView>
        </ScrollView>
    )
}

const styles = StyleSheet.create({
    container: {
        ...StyleSheet.absoluteFillObject,
        height: '100%',
        width: '100%',
        justifyContent: 'flex-end',
        alignItems: 'center',
    },
    map: {
        ...StyleSheet.absoluteFillObject,
    }
});

export default MapStore