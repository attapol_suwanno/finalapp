const innitalstate = {
    store: [{
        store_id: 1,
        store_Name: "Otteri WASH & DRY",
        store_location: {
            0: 18.787500,
            1: 98.954543
        },
        store_image: {
            0:"https://scontent.fbkk1-4.fna.fbcdn.net/v/t31.0-8/22550551_1722289577789732_1456344577286129365_o.jpg?_nc_cat=109&_nc_sid=6e5ad9&_nc_eui2=AeGcB6vjKnqD9djeW_p-np4BjL9aMSA_icEM_ZgjTJJi8nzyyBSFDNdrrV3qktbozqWQMumyda-Ktb5bjwRPrPayRmrPMjZZVTc5i6UNlY0zow&_nc_ohc=zBHsXwKERnIAX8xRTg5&_nc_pt=1&_nc_ht=scontent.fbkk1-4.fna&oh=752ac9332ce0e961ea2c11b94000f54e&oe=5E9B25EC",
            1:"https://lh3.googleusercontent.com/proxy/ZLjftCqJGN1Yrx0IIIxwIJ2yJ0Q_7UXPEJH2z_aKGwOVZ2V0qsSrVTuKJAygv9KiHyJ9wnntWvH5LBsqS7uZfY1FnxwUrpPVfisqvF6Hhp2C4kYJzeqStt2xcL7_Pd8SmnEA04Tm8P6vePBckUNuQtQCmccSPEBHk2a2yy1oqWZPqhT7Chnz646iXFVy8At7OpeiUt6azUFZTgIIzXBzULjgYKg3sx0NR-x654Dav1cXfVOEfvnsFBAtxgljNHXeJDiWWq98TxpAvUibu3Nd"
        },
        promotion: 20,
        level:5.0,
        price: {
            0: 30,
            1: 40,
            2: 60
        },
        tumble_dry: 40,
        detergent: 20
    },
    {
        store_id: 2,
        store_Name:"Kolae Wash & Dry",
        store_location: {
            0: 18.789156,
            1: 98.951705
        },
        store_image: {
            0:"https://scontent.fbkk1-4.fna.fbcdn.net/v/t1.0-0/p640x640/83949022_3406112706125517_1805649335510106112_o.jpg?_nc_cat=109&_nc_sid=110474&_nc_eui2=AeEwBbzh8BH2x2F5JhU5oZRDfg3Lr7EFB_shYpl0SAjAWNzFpuW_2xcw45WBiidaVq3uqrk9YtRWKLrj1WOdFQQcI6xP0rEUnO2s6-lsLn6EEA&_nc_ohc=3s2dV2DyvB8AX99118-&_nc_pt=1&_nc_ht=scontent.fbkk1-4.fna&_nc_tp=6&oh=708a002249b47083b29f078a6a8cec83&oe=5E9C94C1",
            1:"https://scontent.fbkk1-4.fna.fbcdn.net/v/t1.0-0/p640x640/89539170_3561475087255944_6590902807799267328_o.jpg?_nc_cat=109&_nc_sid=8024bb&_nc_eui2=AeH5u9niYYa2aFq_0eez3eDrn_2y1AA2suEznEAkrnpQG8rtdq40k7wpl3AWpHBJ9rz5EBO-INYa11jCUAejOh7pjGYgZQyztwNWtLh8VIjlFw&_nc_ohc=j6lqlMyXDCEAX-5YjmZ&_nc_pt=1&_nc_ht=scontent.fbkk1-4.fna&_nc_tp=6&oh=99cd496b615ad001028a1014e33942e6&oe=5E9BDAA1"
        },
        promotion: 20,
        level:4.5,
        price: {
            0: 30,
            1: 50,
            2: 60
        },
        tumble_dry: 40,
        detergent: 20
    },
    {
        store_id: 3,
        store_Name: "mama",
        store_location: {
            0: 18.791115,
            1:98.955375
        },
        store_image: {
            0:"https://scontent.fbkk1-4.fna.fbcdn.net/v/t1.0-0/p640x640/83949022_3406112706125517_1805649335510106112_o.jpg?_nc_cat=109&_nc_sid=110474&_nc_eui2=AeEwBbzh8BH2x2F5JhU5oZRDfg3Lr7EFB_shYpl0SAjAWNzFpuW_2xcw45WBiidaVq3uqrk9YtRWKLrj1WOdFQQcI6xP0rEUnO2s6-lsLn6EEA&_nc_ohc=3s2dV2DyvB8AX99118-&_nc_pt=1&_nc_ht=scontent.fbkk1-4.fna&_nc_tp=6&oh=708a002249b47083b29f078a6a8cec83&oe=5E9C94C1",
            1:"https://scontent.fbkk1-4.fna.fbcdn.net/v/t1.0-0/p640x640/89539170_3561475087255944_6590902807799267328_o.jpg?_nc_cat=109&_nc_sid=8024bb&_nc_eui2=AeH5u9niYYa2aFq_0eez3eDrn_2y1AA2suEznEAkrnpQG8rtdq40k7wpl3AWpHBJ9rz5EBO-INYa11jCUAejOh7pjGYgZQyztwNWtLh8VIjlFw&_nc_ohc=j6lqlMyXDCEAX-5YjmZ&_nc_pt=1&_nc_ht=scontent.fbkk1-4.fna&_nc_tp=6&oh=99cd496b615ad001028a1014e33942e6&oe=5E9BDAA1"
        },
        promotion: 20,
        level:4.0,
        price: {
            0: 30,
            1: 40,
            2: 30
        },
        tumble_dry: 40,
        detergent: 20
    }]
};

export const storeReducer = (state = innitalstate, action) => {
    switch (action.type) {
              case 'DATA_STORE':
            return { 
                store:action.payLoad
            }

        // case 'DATA_STORE':
        //     return {
        //         ...state,
        //         alltransection: [
        //           ...state.alltransection,
        //           {
        //             laun_id: action.addTranAmont,
        //             store_id: action.addTranname,
        //             price: action.addTime,
        //             tumble_dry: action.addChecked,
        //             detergent: action.addiduser
        //           }
        //         ]
        //       }
        default:
            return state
    }
};