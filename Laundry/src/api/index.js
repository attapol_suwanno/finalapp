import axios from "axios";

// export const host = "http://10.80.20.172:8080";
export const host = "https://luandry.mooo.com";

export function ListStoreAll(data) {
    return axios.get(`${host}/stores`, {
        params: {
            search_input: data
        }
    });
}

export function getLocationStore(id) {
    return axios.get(`${host}/location/${id}`);
}

export function getStorebyId(id) {
    return axios.get(`${host}/store/${id}`);
}

export function SearchNameStore(data) {
    return axios.get(`${host}/searchname/${data}`);
}

export function CreateOrder(id, checked, checkDryValue, checkdetergentValue, checkFabrisoftenerValue, token) {
    console.log('test1', id)
    console.log('test2', checked)
    console.log('test3', checkDryValue)
    console.log('test4', checkdetergentValue)
    console.log('test5', checkFabrisoftenerValue)
    console.log('token', token)
    return axios.post(`${host}/auth/order`, { Storeid: id, Price: checked, Dry: checkDryValue, Detergent: checkdetergentValue, Fabricsof: checkFabrisoftenerValue }, {
        headers: {
            Authorization: `Bearer ${token}`
        }
    });
}

export function deleteOrder(id, token) {
    return axios.delete(`${host}/auth/orders/${id}`, {
        headers: {
            Authorization: `Bearer ${token}`
        }
    });
}

export function ListOrederUser(token) {
    return axios.get(`${host}/auth/orders/user_id`, {
        headers: {
            Authorization: `Bearer ${token}`
        }
    });
}

export function GetListDetailOrderUser(id, token) {
    return axios.get(`${host}/auth/orderd/${id}`, {
        headers: {
            Authorization: `Bearer ${token}`
        }
    });
}

export function ShowhistoryUserOrder(token) {
    return axios.get(`${host}/auth/history`, {
        headers: {
            Authorization: `Bearer ${token}`
        }
    });
}


export function ShoworderInPartner(token) {
    console.log('token Partner', token)
    return axios.get(`${host}/authp/orderallparner`, {
        headers: {
            Authorization: `Bearer ${token}`
        }
    });
}

export function showdetailpartner(id, token) {
    return axios.get(`${host}/authp/orderde/${id}`, {
        headers: {
            Authorization: `Bearer ${token}`
        }
    });
}

export function AcceptToOrederInPartner(id, token) {
    console.log("idddddd--->", id)
    return axios.patch(`${host}/authp/orderupdate/${id}`, {}, {
        headers: {
            Authorization: `Bearer ${token}`
        }
    });
}

export function ShowOrderPartner(token) {
    return axios.get(`${host}/authp/orderallpartnertlist`, {
        headers: {
            Authorization: `Bearer ${token}`
        }
    });
}

export function ShowOrderDetailPartner(id, token) {
    return axios.get(`${host}/authp/orderdetail/${id}`, {
        headers: {
            Authorization: `Bearer ${token}`
        }
    });
}

export function ShowHistoryPartner(token) {
    return axios.get(`${host}/authp/historyPartner`, {
        headers: {
            Authorization: `Bearer ${token}`
        }
    });
}

export function updateStepFinishOrder(id, token) {
    return axios.patch(`${host}/authp/orderupdateFinish/${id}`, {}, {
        headers: {
            Authorization: `Bearer ${token}`
        }
    });
}

export function loginUser(username, password) {
    return axios.post(`${host}/login`, { email: username, password: password });
}

export function SignUpUser(username, email, phonenumber, password, address) {
    return axios.post(`${host}/singup`, { username: username, email: email, password: password, phone: phonenumber, address: address });
}

export function loginPartner(username, password) {
    return axios.post(`${host}/loginpartner`, { email: username, password: password });
}

export function LogoutUserG(token) {
    return axios.post(`${host}/auth/logout`, {}, {
        headers: {
            Authorization: `Bearer ${token}`
        }
    });
}

export function GetProfileUser(token) {
    return axios.get(`${host}/auth/profile`, {
        headers: {
            Authorization: `Bearer ${token}`
        }
    });
}

export function Logoutpartner(token) {
    return axios.post(`${host}/authp/logoutPartner`, {}, {
        headers: {
            Authorization: `Bearer ${token}`
        }
    });
}