import React, { useState, useEffect } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Animated, } from 'react-native';
import { DrawerContentScrollView, } from '@react-navigation/drawer';
import { Button, Title, Avatar, Divider } from 'react-native-paper';
import { connect } from 'react-redux';
import { logOut } from '../action/LoginAction';
import { LogoutUserG } from '../api/index'

const DrawerUser = (props) => {
    const { tokenUser } = props;

    const clicklogout = () => {
        LogoutUserG(tokenUser)
            .then(response => {
                props.logOut()
                console.log('logout User ', response.data)
            })
            .catch(error => {
                console.log('eror', JSON.stringify(error))
            })
    }

    const renderButton = () => {
        if (tokenUser != "") {
            return <View style={{ flex: 0, flexDirection: 'column', justifyContent: 'center', marginTop: '4%', }}  >
                <View style={{ flex: 0, flexDirection: 'row', justifyContent: 'center', }}  >
                    <TouchableOpacity color="#18ffff" icon="logout-variant" mode="contained" onPress={() => props.navigation.navigate('Profile')}  >
                        <Avatar.Image size={70}
                            source={{ uri: 'https://image.flaticon.com/icons/png/512/2990/2990537.png' }}
                        />
                    </TouchableOpacity>
                </View>
                <View style={{ flex: 0, flexDirection: 'row', justifyContent: 'center', marginTop: '4%' }}  >
                    {/* <Title>{DataOneProfile.username}</Title> */}
                </View>
            </View>
        }
    }
    const buttonOnLogin = () => {
        if (tokenUser != "") {
            return (
                <View>
                    <View style={{ flex: 0, flexDirection: 'row', justifyContent: 'center', marginTop: '5%' }}  >
                        <Title>Profile</Title>
                    </View>
                    <Divider />
                    <View style={{ flex: 0, flexDirection: 'row', justifyContent: 'flex-start', marginTop: '5%' }}  >
                        <Button onPress={() => props.navigation.navigate('Profile')} color="black" title="Go back home" icon="account" >
                            <Text>My profile</Text>
                        </Button>
                    </View>
                    <View style={{ flex: 0, flexDirection: 'row', justifyContent: 'flex-start', marginTop: '5%' }}  >
                        <Button onPress={() => props.navigation.navigate('OrderListUser')} color="black" title="Go back home" icon="checkbox-multiple-blank" >
                            <Text>My orders</Text>
                        </Button>
                    </View>
                    <View style={{ flex: 0, flexDirection: 'row', justifyContent: 'flex-start', marginTop: '5%' }}  >
                        <Button onPress={() => props.navigation.navigate('History')} color="black" title="Go back home" icon="history" >
                            <Text>My history</Text>
                        </Button>
                    </View>
                    <Divider />
                    <View style={{ flex: 0, flexDirection: 'row', justifyContent: 'flex-start', marginTop: '5%' }}  >
                        <Button onPress={() => clicklogout()} color="black" title="Go back home" icon="logout-variant" >
                            <Text>Log out</Text>
                        </Button>
                    </View>
                </View>
            )
        } else {
            return (
                <View>
                    <View style={{ flex: 0, flexDirection: 'row', justifyContent: 'flex-start', marginTop: '5%' }}  >
                        <Button onPress={() => props.navigation.navigate('Login')} color="black" title="Go back home"  >
                            <Text>Login / Create account</Text>
                        </Button>
                    </View>
                </View>
            )
        }
    }

    return (
        <DrawerContentScrollView  {...props} >
            {renderButton()}
            {buttonOnLogin()}
        </DrawerContentScrollView>
    )
}

const styles = StyleSheet.create({
    button: {
        zIndex: 0,
        borderRadius: 25,
        width: "60%",
        height: "100%",
        justifyContent: 'center',
        elevation: 25,
        height: 45
    },
    buttonLogout: {
        zIndex: 0,
        width: "60%",
        height: "100%",
        justifyContent: 'center',
        height: 45
    }
});

const mapStateToProps = state => {
    return {
        tokenUser: state.login.token,
    }
}

const mapDispatchToProps = {
    logOut,
}

export default connect(mapStateToProps, mapDispatchToProps)(DrawerUser)