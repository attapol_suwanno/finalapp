
import * as React from 'react';
import { createDrawerNavigator } from '@react-navigation/drawer';
import Home from  '../screen/home/Home';
import DrawerUser from '../component/DrawerUser'


const mainRouter=()=> {
      const Drawer = createDrawerNavigator();
        return (
            <Drawer.Navigator initialRouteName="Home" drawerContent={props=> <DrawerUser {...props} />} >
               <Drawer.Screen name="Home" component={Home} />
            </Drawer.Navigator>
            
        );
}
export default mainRouter;


