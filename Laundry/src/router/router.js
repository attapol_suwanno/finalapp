import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import StoreDetail from '../screen/storeDetail/StoreDetail';
import Login from '../screen/login/Login';
import Register from '../screen/register/Register';
import mainRouter from '../router/mainRouter';
import MapAllStore from '../screen/mapAllStore/MapAllStore';
import MapStore from '../screen/mapStore/MapStore';
import FirstUseApp from '../screen/firstUseApp/FirstUseApp';
import ListOrderPartner from '../screenPartner/listOrderPartner/ListOrderPartner';
import ListTransection from '../screenPartner/listTransection/ListTransection';
import HistoryPartner from '../screenPartner/historyPartner/HistoryParner';
import Profile from '../screen/profile/Profile';
import mainPartnerRouter from '../router/mainPartnerRouter';
import ListDetail from '../screenPartner/listDetail/ListDetail';
import OrderListUser from '../screen/orderListUser/OrderListUser';
import OrderDetail from '../screen/orderDetail/OrderDetail';
import History from '../screen/history/History';
import Home from '../screen/home/Home';
import ProcessPartner from '../screenPartner/processPartner/ProcessPartner';
import LoginPartner from '../screenPartner/loginPartner/loginPartner'



const Stack = createStackNavigator();

export default function Router() {
    return (
        <NavigationContainer>
            <Stack.Navigator>
                <Stack.Screen name="FirstUseApp" component={FirstUseApp} options={{ headerShown: false }} />
                <Stack.Screen name="Login" component={Login} options={{ headerShown: false }} />
                <Stack.Screen name="mainRouter" component={mainRouter} options={{ headerShown: false }} />
                <Stack.Screen name="StoreDetail" component={StoreDetail} options={{ headerShown: false }} />
                <Stack.Screen name="Register" component={Register} options={{ headerShown: false }} />
                <Stack.Screen name="MapAllStore" component={MapAllStore} options={{ headerShown: false }} />
                <Stack.Screen name="MapStore" component={MapStore} options={{ headerShown: false }} />
                <Stack.Screen name="ListOrderPartner" component={ListOrderPartner} options={{ headerShown: false }} />
                <Stack.Screen name="ListTransection" component={ListTransection} options={{ headerShown: false }} />
                <Stack.Screen name="HistoryPartner" component={HistoryPartner} options={{ headerShown: false }} />
                <Stack.Screen name="Profile" component={Profile} options={{ headerShown: false }} />
                <Stack.Screen name="mainPartnerRouter" component={mainPartnerRouter} options={{ headerShown: false }} />
                <Stack.Screen name="ListDetail" component={ListDetail} options={{ headerShown: false }} />
                <Stack.Screen name="OrderListUser" component={OrderListUser} options={{ headerShown: false }} />
                <Stack.Screen name="OrderDetail" component={OrderDetail} options={{ headerShown: false }} />
                <Stack.Screen name="History" component={History} options={{ headerShown: false }} />
                <Stack.Screen name="Home" component={Home} options={{ headerShown: false }} />
                <Stack.Screen name="ProcessPartner" component={ProcessPartner} options={{ headerShown: false }} />
                <Stack.Screen name="LoginPartner" component={LoginPartner} options={{ headerShown: false }} />
            </Stack.Navigator>
        </NavigationContainer>
    );
}