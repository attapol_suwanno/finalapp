
import * as React from 'react';
import { createDrawerNavigator } from '@react-navigation/drawer';
import HomeListPartner from '../screenPartner/homeListPartner/HomeListPartner'
import DrawerPartner from '../component/DrawerPartner'


const mainPartnerRouter=()=> {
      const Drawer = createDrawerNavigator();
        return (
            <Drawer.Navigator initialRouteName="HomeListPartner" drawerContent={props=> <DrawerPartner {...props} />} >
               <Drawer.Screen name="HomeListPartner" component={HomeListPartner} />
            </Drawer.Navigator>
        );
}
export default mainPartnerRouter;


